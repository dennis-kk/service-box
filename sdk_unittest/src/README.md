# 测试框架启动配置

```

// 注意: 将此文件放到可执行文件所在目录

sdk = {
   remote_host = "127.0.0.1" // 连接到服务所在进程IP
   remote_port = 6888 // 连接到服务所在进程端口
   conn_timeout = 2000 // 连接超时，毫秒，自动重连
   
   // 日志配置
   log_cfg = "file://.;file=sdk_%y%m%d.log;line=[%y%m%d-%H:%M:%S:%U];flush=true;mode=day;maxSize=10240000"
   // SDK plugin存放目录
   plugin_dir = "..\..\..\src\repo\lib\proxy\example\ServiceDynamic\Debug"
}

```


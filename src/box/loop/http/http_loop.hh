﻿#pragma once

#include "box/loop/loop.hh"
#include "util/box_std_allocator.hh"
#include "util/write_buffer.hh"

// 前置声明，依赖knet
typedef struct _loop_t kloop_t;
typedef struct _channel_ref_t kchannel_ref_t;
// 前置声明，依赖http_parser, http_parser_settings
struct http_parser;
struct http_parser_settings;

namespace kratos {
namespace service {
class BoxNetwork;
}
} // namespace kratos

namespace kratos {
namespace loop {

class JsonPb;

/**
 * @brief HTTP网络循环实现
 */
class HttpLoop : public Loop {
public:
  /**
   * 管道信息.
   */
  struct ChannelInfo {
    /**
     * 构造
     *
     * \param channel_shared
     */
    ChannelInfo(kchannel_ref_t *channel_shared);
    /**
     * 析构
     */
    ~ChannelInfo();
    kchannel_ref_t *channel{nullptr};                  ///< 网络管道
    std::unique_ptr<http_parser> parser;               ///< HTTP协议解析
    std::unique_ptr<http_parser_settings> settings;    ///< HTTP解析器回调
    std::unique_ptr<kratos::util::WriteBuffer> buffer; ///< 写入缓存
    using Header = service::PoolUnorederedMap<std::string, std::string>;
    Header header;              ///< HTTP header, reserved
    std::string body;           ///< HTTP body
    std::string last_key;       ///< 当前正在解析的header key
    std::string uri;            ///< URI
    std::uint64_t uuid{0};      ///< 当前调用的服务UUID
    std::uint16_t method_id{0}; ///< 当前调用的方法ID
  };

private:
  service::BoxNetwork *network_{nullptr}; ///< 网络框架指针
  kloop_t *loop_{nullptr};                ///< 网络循环
  std::unique_ptr<JsonPb> json_pb_;       ///< JsonPb实例

  using ChannelInfoPtr = std::shared_ptr<HttpLoop::ChannelInfo>;
  using ChannelMap = service::PoolUnorederedMap<std::uint64_t, ChannelInfoPtr>;
  ChannelMap thread_channel_map_; ///< 网络线程管道查找表

public:
  HttpLoop(service::BoxNetwork *network);
  virtual ~HttpLoop();
  virtual auto start() -> bool override;
  virtual auto stop() -> bool override;
  virtual auto worker_update(kratos::service::BoxNetwork *network_ptr)
      -> void override;
  virtual auto do_worker_event(const service::NetEventData &event_data)
      -> void override;
  virtual auto worker_cleanup() -> void override;

public:
  /**
   * @brief 获取网络框架
   * @return 网络框架指针
   */
  auto get_network() -> service::BoxNetwork *;
  /**
   * 获取工作线程的管道表容器
   *
   * \return 管道表容器
   */
  auto get_worker_channel_map() noexcept -> ChannelMap &;
  /**
   * 获取JsonPb.
   *
   * \return
   */
  auto get_json_pb() -> JsonPb *;

public:
  //
  // 以下函数为http_parser_settings的回调函数，具体含义参见@see
  // http_parser_settings
  //

  static int on_message_begin(http_parser *);
  static int on_message_complete(http_parser *);
  static int on_url(http_parser *, const char *at, std::size_t length);
  static int on_header_field(http_parser *, const char *at, std::size_t length);
  static int on_header_value(http_parser *, const char *at, std::size_t length);
  static int on_status(http_parser *, const char *at, std::size_t length);
  static int on_headers_complete(http_parser *);
  static int on_body(http_parser *, const char *at, std::size_t length);
  static int on_chunk_header(http_parser *);
  static int on_chunk_complete(http_parser *);
};

} // namespace loop
} // namespace kratos

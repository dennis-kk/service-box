﻿#pragma once

#include <atomic>
#include <google/protobuf/compiler/importer.h>
#include <google/protobuf/dynamic_message.h>
#include <google/protobuf/message.h>
#include <google/protobuf/reflection.h>
#include <memory>
#include <string>
#include <thread>

#include "util/directory_watcher.hpp"
#include "util/method_type.hh"
#include "util/spsc_queue.hpp"
#include "util/write_buffer.hh"
#include "json/json.h"

namespace kratos {
namespace service {
class BoxNetwork;
}
} // namespace kratos

namespace kratos {
namespace util {
class WriteBuffer;
}
} // namespace kratos

namespace Json {
class Value;
}

namespace kratos {
namespace loop {

/**
 * JSON与protobuf::Message之间的转换工具.
 */
class JsonPb {
  std::unique_ptr<kratos::util::MsgFactory> msg_factory_ptr_; ///< 消息工厂
  kratos::service::BoxNetwork *network_{nullptr};             ///< 容器
  kratos::util::WriteBuffer buffer_;                          ///< 写入缓存
  kratos::corelib::SPSCQueue<int> update_event_queue_; ///< 热更新通知队列
  std::thread update_worker_;       ///< 热更新检测线程
  std::atomic_bool running_{false}; ///< 线程运行标志
  std::unique_ptr<kratos::util::DirectoryWatcher> watcher_; ///< 目录监控
  const std::time_t DEFAULT_INTVAL = 30;
  std::time_t check_intval_{DEFAULT_INTVAL}; ///< 默认目录检测时间秒

public:
  /**
   * 构造.
   *
   * \param network BoxNetwork
   */
  JsonPb(kratos::service::BoxNetwork *network);
  /**
   * 析构.
   *
   */
  ~JsonPb();
  /**
   * 加载配置并初始化资源.
   *
   * \return true or false
   */
  auto load() noexcept(false) -> bool;
  /**
   * 将JSON串转化为protobuf::Message对象并序列化到缓冲区.
   *
   * \param value JSON对象
   * \param [IN OUT] rpc_proto 缓冲区地址, 此地址不能保存
   * \param [IN OUT] length 缓冲区长度, 此地址长度不能保存
   * \param method_type_ptr MethodType指针
   * \return true or false
   */
  auto to_rpc(Json::Value &value, char *&rpc_proto, std::size_t &length,
              const util::MethodType *method_type_ptr) noexcept(false) -> bool;
  /**
   * 将JSON串转化为protobuf::Message对象并序列化到缓冲区.
   *
   * \param value JSON对象
   * \param [IN OUT] rpc_proto 缓冲区地址, 此地址不能保存
   * \param [IN OUT] length 缓冲区长度, 此地址长度不能保存
   * \param method_type_ptr MethodType指针
   * \param ctx_items 上下文
   * \return true or false
   */
  template <typename ContT>
  auto to_rpc(Json::Value &value, char *&rpc_proto, std::size_t &length,
              const util::MethodType *method_type_ptr,
              const ContT &ctx_items) noexcept(false) -> bool;
  /**
   * 将protobuf::Message数据流转化为一个JSON对象.
   *
   * \param uuid 服务UUID
   * \param method_id 方法ID
   * \param [IN OUT] value JSON对象
   * \param rpc_proto protobuf::Message数据流
   * \param length protobuf::Message数据流长度
   * \return true or false
   */
  auto to_json(rpc::ServiceUUID uuid, rpc::MethodID method_id,
               Json::Value &value, const char *rpc_proto,
               std::size_t length) noexcept(false) -> bool;
  /**
   * 获取{rpc::ServiceUUID, rpc::MethodID}.
   *
   * \param service_name 服务名
   * \param method_name 方法名
   * \return {rpc::ServiceUUID, rpc::MethodID}
   */
  auto get_method_info(const std::string &service_name,
                       const std::string &method_name)
      -> std::pair<rpc::ServiceUUID, rpc::MethodID>;
  /**
   * 获取是否是oneway方法
   *
   * \param uuid 服务UUID
   * \param method_id 方法ID
   * \return true or false
   */
  auto is_oneway(rpc::ServiceUUID uuid, rpc::MethodID method_id) -> bool;

  /**
   * 获取方法原型信息
   *
   * \param uuid 服务UUID
   * \param method_id 方法ID
   * \return MethodType指针
   */
  auto get_method_type(rpc::ServiceUUID uuid, rpc::MethodID method_id)
      -> const util::MethodType *;

private:
  /**
   * 检查更新
   */
  auto check_update() -> void;
  /**
   * 将JSON对象转换为ProtobufMessage.
   *
   * \param value JSON对象
   * \param msg ProtobufMessage
   * \return true or false
   */
  auto encode(const Json::Value &value, ProtobufMessage *msg) -> bool;
  /**
   * 将ProtobufMessage转换为JSON对象.
   *
   * \param pb_msg ProtobufMessage
   * \return JSON对象
   */
  auto decode(const ProtobufMessage &pb_msg) -> std::unique_ptr<Json::Value>;
  //
  // 以下为JSON和Protobuf间的转换函数
  //
  auto on_field(const ProtobufMessage &msg,
                const ProtobufFieldDescriptor *field)
      -> std::unique_ptr<Json::Value>;
  auto on_map_field(const ProtobufMessage &msg,
                    const ProtobufFieldDescriptor *field)
      -> std::unique_ptr<Json::Value>;
  auto on_map_field_key(const ProtobufMessage &msg,
                        const ProtobufFieldDescriptor *field)
      -> std::unique_ptr<Json::Value>;
  auto on_map_field_value(const ProtobufMessage &msg,
                          const ProtobufFieldDescriptor *field)
      -> std::unique_ptr<Json::Value>;
  auto on_repeated_field(const ProtobufMessage &msg,
                         const ProtobufFieldDescriptor *field)
      -> std::unique_ptr<Json::Value>;
  auto on_object_value(const Json::Value &obj_ptr, ProtobufMessage &msg,
                       const ProtobufReflection *ref,
                       const ProtobufFieldDescriptor *field) -> void;
  auto on_object_map(const Json::Value &obj_ptr, ProtobufMessage &msg,
                     const ProtobufReflection *ref,
                     const ProtobufFieldDescriptor *field) -> void;
  auto on_object_repeated_value(const Json::Value &obj_ptr,
                                ProtobufMessage &msg,
                                const ProtobufReflection *ref,
                                const ProtobufFieldDescriptor *field) -> void;
};

template <typename ContT>
auto JsonPb::to_rpc(Json::Value &value, char *&rpc_proto, std::size_t &length,
                    const util::MethodType *method_type_ptr,
                    const ContT &ctx_items) noexcept(false) -> bool {
  check_update();
  if (!value.isMember("args")) {
    throw std::runtime_error("Invalid format, need 'args'");
    return false;
  }
  auto &arg_node = value["args"];
  auto arg_count = std::size_t(arg_node.size());
  if (arg_count != method_type_ptr->arg_name_vector.size()) {
    throw std::runtime_error(
        "Invalid argument number, need " +
        std::to_string(method_type_ptr->arg_name_vector.size()) + " but " +
        std::to_string(arg_count) + "given");
    return false;
  }
  for (const auto &arg_name : method_type_ptr->arg_name_vector) {
    if (arg_node.isMember(arg_name.pb_name)) {
      // JSON内字段存在这个PB参数名字
      continue;
    }
    if (!arg_node.isMember(arg_name.decl_name)) {
      // 没有PB名字也没有形参名
      throw std::runtime_error("Method argument name invalid");
      return false;
    }
    // 处理不存在的情况
    arg_node[arg_name.pb_name] = arg_node[arg_name.decl_name];
    arg_node.removeMember(arg_name.decl_name);
  }
  auto *msg_ptr = msg_factory_ptr_->new_call_param(
      method_type_ptr->service_name, method_type_ptr->method_name);
  if (!msg_ptr) {
    throw std::runtime_error("Method not found, " +
                             method_type_ptr->service_name + ":" +
                             method_type_ptr->method_name);
    return false;
  }
  if (!encode(arg_node, msg_ptr)) {
    return false;
  }
  if (!ctx_items.empty()) {
    const auto *field = msg_ptr->GetDescriptor()->FindFieldByName("context");
    const auto *reflection = msg_ptr->GetReflection();
    if (field) {
      Json::Value header;
      for (const auto &[k, v] : ctx_items) {
        header[k] = v;
      }
      //
      // 写入context, http-header : json string
      //
      // Context
      auto *ctx_ptr = reflection->MutableMessage(msg_ptr, field);
      const auto *ctx_ref = ctx_ptr->GetReflection();
      // info
      const auto *key_value_desc = ctx_ptr->GetDescriptor()->field(0);
      auto *key_value_ptr = ctx_ref->AddMessage(ctx_ptr, key_value_desc);
      const auto *key_value_ref = key_value_ptr->GetReflection();
      //
      // key, value
      //
      const auto *key_desc = key_value_ptr->GetDescriptor()->field(0);
      const auto *value_desc = key_value_ptr->GetDescriptor()->field(1);
      key_value_ref->SetString(key_value_ptr, key_desc, "http-header");
      key_value_ref->SetString(key_value_ptr, value_desc,
                               header.toStyledString());
    }
  }
  buffer_.clear();
  auto res = buffer_.apply(msg_ptr->ByteSizeLong());
  if (!msg_ptr->SerializeToArray(res.first, (int)res.second)) {
    throw std::runtime_error("Invalid format, SerializeToArray");
    return false;
  }
  rpc_proto = res.first;
  length = res.second;
  return true;
}

} // namespace loop
} // namespace kratos

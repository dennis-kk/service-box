﻿#include "plugin_loader.hh"
#include <stdexcept>

namespace kratos {
namespace sdk {

// Plugin导出函数名
constexpr auto *CLASS_PROXY_REGISTER_NAME = "registerProxyCreator";
constexpr auto *CLASS_STUB_REGISTER_NAME = "registerStubCreator";

PluginLoader::PluginLoader(const std::string &file_path) noexcept(false) {
#if defined(_WIN32) || defined(_WIN64)
  handle_ = ::LoadLibraryA(file_path.c_str());
#else
  handle_ = dlopen(file_path.c_str(), RTLD_LAZY);
#endif // defined(_WIN32) || defined(_WIN64)
  if (handle_ == INVALID_MODULE_HANDLE) {
    throw std::runtime_error("Load SDK plugin failed");
  }
#if defined(_WIN32) || defined(_WIN64)
  reg_func_ =
      (ClassRegisterFunc)::GetProcAddress(handle_, CLASS_PROXY_REGISTER_NAME);
  if (!reg_func_) {
    reg_func_ =
        (ClassRegisterFunc)::GetProcAddress(handle_, CLASS_STUB_REGISTER_NAME);
  }
#else
  reg_func_ = (ClassRegisterFunc)dlsym(handle_, CLASS_PROXY_REGISTER_NAME);
  if (!reg_func_) {
    reg_func_ = (ClassRegisterFunc)dlsym(handle_, CLASS_STUB_REGISTER_NAME);
  }
#endif // defined(_WIN32) || defined(_WIN64)
  if (!reg_func_) {
    throw std::runtime_error("SDK plugin entry not found");
  }
  load_flag_ = true;
}

PluginLoader::PluginLoader(PluginLoader &&cl) noexcept(true) {
  handle_ = cl.handle_;
  reg_func_ = cl.reg_func_;
  load_flag_ = cl.load_flag_;
  cl.handle_ = INVALID_MODULE_HANDLE;
  cl.reg_func_ = nullptr;
  cl.load_flag_ = false;
}

PluginLoader::~PluginLoader() { unload(); }

auto PluginLoader::unload() -> void {
  if (!load_flag_ || (handle_ == INVALID_MODULE_HANDLE)) {
    return;
  }
#if defined(_WIN32) || defined(_WIN64)
  if (FALSE == ::FreeLibrary(handle_)) {
    if (FALSE == ::UnmapViewOfFile(handle_)) {
      return;
    }
  }
#else
  dlclose(handle_);
#endif // defined(_WIN32) || defined(_WIN64)
}

auto PluginLoader::do_register(rpc::Rpc *rpc) -> void {
  if (!load_flag_ || !reg_func_) {
    return;
  }
  // 调用导出的注册函数
  reg_func_(rpc);
}

auto PluginLoader::get_uuid(const std::string &file_path) -> std::uint64_t {
#if defined(_WIN32) || defined(_WIN64)
  auto handle = ::LoadLibraryA(file_path.c_str());
#else
  auto handle = dlopen(file_path.c_str(), RTLD_LAZY);
#endif // defined(_WIN32) || defined(_WIN64)
  if (handle == INVALID_MODULE_HANDLE) {
    throw std::runtime_error("Load SDK plugin failed");
  }
#if defined(_WIN32) || defined(_WIN64)
  auto reg_func = (rpc::BundleRegisterFunc)::GetProcAddress(
      handle, rpc::DEFAULT_REGISTER_NAME);
#else
  auto reg_func = (rpc::BundleRegisterFunc)dlsym(handle, rpc::DEFAULT_REGISTER_NAME);
#endif
  if (!reg_func) {
    return 0;
  }
  return reg_func(nullptr);
}

} // namespace sdk
} // namespace kratos

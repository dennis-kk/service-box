﻿#pragma once

#include "root/rpc_defines.h"
#include "root/rpc_proxy.h"
#include "root/rpc_root.h"
#include "root/rpc_logger.h"

namespace kratos {
namespace sdk {

/**
 * Plugin加载类.
 */
class PluginLoader {
  PluginLoader(const PluginLoader &) = delete;
  PluginLoader &operator=(const PluginLoader &) = delete;

  ModuleHandle handle_{INVALID_MODULE_HANDLE}; ///< plugin对应的资源句柄
  /**
   * Plugin导出函数原型.
   */
  using ClassRegisterFunc = void (*)(rpc::Rpc *);
  ClassRegisterFunc reg_func_{nullptr}; ///< 注册函数指针
  bool load_flag_{false};               ///< 加载成功标志

public:
  /**
   * 构造, 发生错误会产生异常.
   *
   * \param file_path Plugin文件路径
   * \return
   */
  PluginLoader(const std::string &file_path) noexcept(false);
  /**
   * 右值构造.
   *
   * \param cl 右值实例
   * \return
   */
  PluginLoader(PluginLoader &&cl) noexcept(true);
  /**
   * 析构.
   *
   */
  ~PluginLoader();
  /**
   * 主动卸载Plugin，如果外部持有会产生不可预期的结果.
   *
   * \return
   */
  auto unload() -> void;
  /**
   * 注册Plugin到SDK框架.
   *
   * \param rpc RPC框架指针
   * \return
   */
  auto do_register(rpc::Rpc *rpc) -> void;
  /**
   * 获取服务UUID.
   *
   * \param file_path Plugin文件路径
   * \return 服务UUID
   */
  static auto get_uuid(const std::string &file_path) -> std::uint64_t;
};

} // namespace sdk
} // namespace kratos

#pragma once

#include "root/rpc_defines.h"

namespace kratos {
namespace service {

class UUIDCreatorImpl : public rpc::UUIDCreator {
public:
  UUIDCreatorImpl();
  virtual ~UUIDCreatorImpl();
  virtual bool generate(std::string &uuid) override;
};

} // namespace service
} // namespace kratos

﻿#include "call_tracer_log_zipkin.hh"
#include "config/box_config.hh"
#include "util/os_util.hh"
#include "util/time_system.hh"
#include "json/json.h"
#include <chrono>
#include <filesystem>
#include <memory>
#include <mutex>
#include <stdexcept>

namespace kratos {
namespace service {

CallTracerLogZipkin::CallTracerLogZipkin() {}

CallTracerLogZipkin::~CallTracerLogZipkin() {
  running_ = false;
  if (worker_.joinable()) {
    // 等待工作线程结束
    worker_.join();
  }
}

auto CallTracerLogZipkin::start(kratos::config::BoxConfig &config) -> bool {
  // 获取日志根目录配置，默认为当前工作目录
  if (config.has_attribute("call_tracer.log.log_root_dir")) {
    log_root_dir_ = config.get_string("call_tracer.log.log_root_dir");
  }
  pid_ = util::get_pid();
  swbuilder_ = std::make_unique<Json::StreamWriterBuilder>();
  // 自定义builder属性, 提升写入性能, 禁用缩进和换行
  swbuilder_->settings_["indentation"] = "";
  swbuilder_->settings_["commentStyle"] = "None";
  swbuilder_->settings_["enableYAMLCompatibility"] = false;
  swwriter_.reset(swbuilder_->newStreamWriter());
  // 启动工作线程
  worker_ = std::thread([this]() {
    running_ = true;
    while (running_) {
      run();
      std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
    // 线程退出前将未写入日志全部写入日志文件
    flush_log();
    if (cur_ofs_) {
      cur_ofs_.close();
    }
  });
  return true;
}
auto CallTracerLogZipkin::trace(const TraceInfo &trace_info) -> void {
  log_queue_.enqueue(new TraceInfo(trace_info));
}

auto CallTracerLogZipkin::update(std::time_t /*now*/) -> void {}

auto CallTracerLogZipkin::run() noexcept(true) -> void { flush_log(); }

auto CallTracerLogZipkin::check_and_switch(std::time_t stamp_sec) noexcept(true)
    -> bool {
  time_system::Datetime date_time(stamp_sec);
  if (last_date_.empty()) {
    // 第一次调用，建立或代开一个新的日志文件
    last_date_ = date_time;
    return new_log_file(last_date_);
  } else if (last_date_.year != date_time.year ||
             last_date_.day != date_time.day ||
             last_date_.hour != date_time.hour) {
    // 记录最新的日期，因为日期不同则建立一个新的文件
    last_date_ = date_time;
    return new_log_file(last_date_);
  }
  return true;
}

auto CallTracerLogZipkin::new_log_file(
    const time_system::Datetime &date_time) noexcept(true) -> bool {
  namespace fs = std::filesystem;
  //
  // 文件名为:年-月-日-小时.pid.trlog
  //
  std::string file_name(
      std::to_string(date_time.year) + "-" + std::to_string(date_time.month) +
      "-" + std::to_string(date_time.day) + "-" +
      std::to_string(date_time.hour) + "." + std::to_string(pid_));
  std::string complete_path =
      (fs::path(log_root_dir_) /= fs::path(file_name)).string() + ".trlog";
  if (cur_ofs_) {
    cur_ofs_.close();
  }
  if (fs::exists(complete_path)) {
    // 文件存在则打开已存在的
    cur_ofs_.open(complete_path, std::ios::app | std::ios::out);
  } else {
    // 文件不存在则建立一个新的
    cur_ofs_.open(complete_path, std::ios::trunc | std::ios::out);
  }
  if (!cur_ofs_) {
    return false;
  }
  return true;
}

auto CallTracerLogZipkin::flush_log() noexcept(true) -> void {
  try {
    //
    // 一次取出队列内所有信息
    //
    TraceInfo *log_ptr{nullptr};
    while (log_queue_.try_dequeue(log_ptr)) {
      write_and_destroy_log(log_ptr);
    }
  } catch (std::exception & /*ex*/) {
    // TODO error
  }
}

auto CallTracerLogZipkin::write_and_destroy_log(
    TraceInfo *&log_raw_ptr) noexcept(false) -> void {
  // 智能指针持有，防止内存泄漏
  std::unique_ptr<TraceInfo> log_ptr(log_raw_ptr);
  // 检查日志文件，如果需要则进行切换, 这里的时间戳是微秒
  if (!check_and_switch(log_ptr->timestamp_ms / std::time_t(1000000))) {
    throw std::runtime_error("Switch log file failed");
  }
  if (!cur_ofs_) {
    return;
  }
  static std::unordered_map<TraceType, std::string> kind_map = {
      {TraceType::NONE, ""},
      {TraceType::CLIENT, "CLIENT"},
      {TraceType::SERVER, "SERVER"},
      {TraceType::PRODUCER, "PRODUCER"},
      {TraceType::CONSUMER, "CONSUMER"},
  };

  static std::unordered_map<TraceDetailType, std::string> detail_map = {
      {TraceDetailType::NONE, ""},
      {TraceDetailType::PROXY_CALL_REMOTE_SERVICE, "Proxy call remote service"},
      {TraceDetailType::PROXY_RECV_SERVICE_RESULT,
       "Proxy received service's result"},
      {TraceDetailType::SERVICE_RECV_PROXY_REQUEST,
       "Service received proxy's request"},
      {TraceDetailType::SERVICE_SEND_PROXY_RESULT,
       "Service sent result to proxy"},
  };

  // 目标格式为JSON格式, 每行日志为一个JSON对象
  // 参考: https://zipkin.io/zipkin-api/
  //
  Json::Value line;
  line["kind"] = kind_map[log_ptr->type];
  line["traceId"] = std::move(log_ptr->trace_id);
  line["id"] = std::move(std::to_string(log_ptr->span_id));
  if (log_ptr->parent_span_id) {
    line["parentId"] = std::move(std::to_string(log_ptr->parent_span_id));
  }
  line["name"] = std::move(log_ptr->service_name + "::" + log_ptr->method_name);
  Json::Value local_end_point;
  if (log_ptr->type == TraceType::CLIENT) {
    local_end_point["serviceName"] =
        std::move(log_ptr->service_name + " proxy");
  } else {
    local_end_point["serviceName"] = std::move(log_ptr->service_name);
  }
  local_end_point["ipv4"] = std::move(log_ptr->from_ip);
  local_end_point["port"] = log_ptr->from_port;
  line["localEndpoint"] = std::move(local_end_point);
  Json::Value remote_end_point;
  if (log_ptr->type == TraceType::CLIENT) {
    remote_end_point["serviceName"] = std::move(log_ptr->service_name);
  } else {
    remote_end_point["serviceName"] =
        std::move(log_ptr->service_name + " proxy");
  }
  remote_end_point["ipv4"] = std::move(log_ptr->target_ip);
  remote_end_point["port"] = log_ptr->target_port;
  line["remoteEndpoint"] = std::move(remote_end_point);
  line["timestamp"] = (std::int32_t)log_ptr->timestamp_ms;
  line["timestamp_millis"] = (std::int32_t)(log_ptr->timestamp_ms / std::time_t(1000));
  line["duration"] = (std::int32_t)log_ptr->duration;
  line["shared"] = true;

#if defined(DEBUG) || defined(_DEBUG)
  Json::Value tags;
  tags["os.pid"] = log_ptr->pid;
  tags["method.args"] = std::move(log_ptr->param_info);
  line["tags"] = std::move(tags);
  Json::Value annotations;
  annotations["timestamp"] = log_ptr->timestamp_ms;
  annotations["value"] = detail_map[log_ptr->detail_type];
  Json::Value annotations_array(Json::ValueType::arrayValue);
  annotations_array.append(std::move(annotations));
  line["annotations"] = std::move(annotations_array);
#endif // defined(DEBUG) || defined(_DEBUG)
  if (!cur_ofs_) {
    return;
  }
  // 写入文件
  swwriter_->write(line, &cur_ofs_);
  cur_ofs_ << std::endl;
  // flush
  cur_ofs_.flush();
}

} // namespace service
} // namespace kratos

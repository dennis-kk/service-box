#include "component_impl.hh"
#include "box/service_box.hh"
#include "config/box_config.hh"
#include "detail/scope_curl.hh"
#include "util/object_pool.hh"
#include "util/os_util.hh"
#include "util/string_util.hh"
#include "json/json.h"
#include <filesystem>
#include <fstream>

namespace kratos {
namespace component {

VersionImpl::VersionImpl() {}
VersionImpl::~VersionImpl() {}
auto VersionImpl::to_string() -> const std::string & { return ver_str_; }
auto VersionImpl::major() const -> std::int32_t { return major_; }
auto VersionImpl::minor() const -> std::int32_t { return minor_; }
auto VersionImpl::patch() const -> std::int32_t { return patch_; }
auto VersionImpl::operator>(const Version &rht) const -> bool {
  const auto &ref = dynamic_cast<const VersionImpl &>(rht);
  if (major_ > ref.major_) {
    return true;
  } else {
    if (major_ < ref.major_) {
      return false;
    }
  }
  if (minor_ > ref.minor_) {
    return true;
  } else {
    if (minor_ < ref.minor_) {
      return false;
    }
  }
  return (patch_ > ref.patch_);
}
auto VersionImpl::operator<(const Version &rht) const -> bool {
  const auto &ref = dynamic_cast<const VersionImpl &>(rht);
  if (major_ < ref.major_) {
    return true;
  } else {
    if (major_ > ref.major_) {
      return false;
    }
  }
  if (minor_ < ref.minor_) {
    return true;
  } else {
    if (minor_ > ref.minor_) {
      return false;
    }
  }
  return (patch_ < ref.patch_);
}
auto VersionImpl::operator==(const Version &rht) const -> bool {
  const auto &ref = dynamic_cast<const VersionImpl &>(rht);
  return (major_ == ref.major_ && minor_ == ref.minor_ && patch_ == ref.patch_);
}
auto VersionImpl::operator>=(const Version &rht) const -> bool {
  const auto &ref = dynamic_cast<const VersionImpl &>(rht);
  if (major_ < ref.major_) {
    return false;
  } else {
    if (major_ > ref.major_) {
      return true;
    }
  }
  if (minor_ < ref.minor_) {
    return false;
  } else {
    if (minor_ > ref.minor_) {
      return true;
    }
  }
  return (patch_ >= ref.patch_);
}
auto VersionImpl::operator<=(const Version &rht) const -> bool {
  const auto &ref = dynamic_cast<const VersionImpl &>(rht);
  if (major_ > ref.major_) {
    return false;
  } else {
    if (major_ < ref.major_) {
      return true;
    }
  }
  if (minor_ > ref.minor_) {
    return false;
  } else {
    if (minor_ < ref.minor_) {
      return true;
    }
  }
  return (patch_ <= ref.patch_);
}
auto VersionImpl::operator!=(const Version &rht) const -> bool {
  const auto &ref = dynamic_cast<const VersionImpl &>(rht);
  return (major_ != ref.major_ || minor_ != ref.minor_ || patch_ != ref.patch_);
}
auto VersionImpl::from_string(const std::string &ver_str, std::string &error)
    -> VersionUniPtr {
  if (ver_str.empty()) {
    error = "invalid version format";
    return nullptr;
  }
  std::vector<std::string> result;
  util::split(ver_str, ".", result);
  if (result.size() != 3) {
    error = "invalid version format";
    return nullptr;
  }
  auto ptr = std::make_unique<VersionImpl>();
  try {
    //
    // 版本号，只支持数字
    //
    ptr->major_ = std::stoi(result[0]);
    ptr->minor_ = std::stoi(result[1]);
    ptr->patch_ = std::stoi(result[2]);
  } catch (...) {
    error = "invalid version format, only support digit";
    return nullptr;
  }
  ptr->ver_str_ = ver_str;
  return ptr;
}
auto VersionImpl::from_number(std::int32_t major, std::int32_t minor,
                              std::int32_t patch) -> VersionUniPtr {
  if (major <= 0 && minor <= 0 && patch <= 0) {
    return nullptr;
  }
  auto ptr = std::make_unique<VersionImpl>();
  ptr->major_ = major;
  ptr->minor_ = minor;
  ptr->patch_ = patch;
  ptr->ver_str_ = std::to_string(major) + "." + std::to_string(minor) + "." +
                  std::to_string(patch);
  return ptr;
}

DescriptorImpl::DescriptorImpl() {}

DescriptorImpl::~DescriptorImpl() {}

auto DescriptorImpl::get_value(const std::string &key) const
    -> const std::string & {
  static std::string NullString;
  auto it = value_map_.find(key);
  if (it == value_map_.end()) {
    return NullString;
  }
  return it->second;
}

auto DescriptorImpl::add_value(const std::string &key, const std::string &value)
    -> void {
  value_map_[key] = value;
}

ComponentImpl::ComponentImpl() : loader_(nullptr) {}

ComponentImpl::~ComponentImpl() {}

auto ComponentImpl::get_version() -> const Version * { return ver_ptr_.get(); }

auto ComponentImpl::get_descriptor() -> const Descriptor * {
  return desc_ptr_.get();
}

auto ComponentImpl::get_func(const std::string &name) -> void * {
  auto it = export_map_.find(name);
  if (it != export_map_.end()) {
    return it->second;
  }
  auto *func = loader_.get_export_func_raw(name);
  if (func) {
    // 记录函数
    export_map_.emplace(name, func);
  }
  return func;
}

auto ComponentImpl::is_system() -> bool { return is_system_; }

auto ComponentImpl::set_system(bool flag) -> void { is_system_ = flag; }

auto ComponentImpl::load(const std::string &path, std::string &error) -> bool {
  // 加载组件入口dll/so
  auto ret = loader_.load(path);
  if (!ret) {
    error = "Load component entry module failed [" + path + "]";
  }
  return ret;
}

auto ComponentImpl::set_version(VersionUniPtr &&ver) -> void {
  ver_ptr_ = std::move(ver);
}

auto ComponentImpl::set_descriptor(DescriptorUniPtr &&desc) -> void {
  desc_ptr_ = std::move(desc);
}

ComponentFactoryImpl::ComponentFactoryImpl() {}

ComponentFactoryImpl::~ComponentFactoryImpl() {}

auto ComponentFactoryImpl::set_root_directory(const std::string &root_directory)
    -> void {
  root_dir_ = root_directory;
}

struct Const {
  constexpr static const char *CONFIG_FILE_NAME = "META.json";
};

//
// Component 目录结构
//      root
//        \
//         |
//         +--- comp_name
//         |             +-- temp
//         |             |      \
//         |             \       +-- pack.json
//         |              \
//         |               +-- 1.0.0
//         |               |      \
//         |               |       +-- META.json
//         |               |       |
//         |               |       +-- MyComp.so
//         |               |       |
//         |               |       +-- ...
//         |               +-- 1.0.1
//         |               |
//         |               +-- ...
//         +-- ...
//

auto ComponentFactoryImpl::load(const std::string &comp_name,
                                std::string &error, const std::string &version)
    -> ComponentPtr {
  namespace fs = std::filesystem;

  //
  // META.json
  // {
  // "version" : "1.0.0",
  // "descriptor" : { "key":"value" },
  // "entry_module" : "MyComp.so",
  // "deps" : [
  //   {
  //     "version" : ">=1.0.1",
  //     "name" : "OtherComp",
  //     "source" : "http://xx.com/path"
  //   }
  // ]
  // }
  //
  // 目录路径:
  // root/comp_name/version/META.json
  //

  //
  // 根路径: root/comp_name
  //
  auto path = root_dir_ / fs::path(comp_name);
  auto version_path = fs::path();
  if (version.empty()) {
    //
    // 没有指定版本, 使用最新版本
    //
    auto latest_ver_ptr = get_highest_version(comp_name);
    if (!latest_ver_ptr) {
      error = "Load component " + comp_name +
              " META.json failed [Version not satisfied " + version + "]";
      return nullptr;
    }
    // root/comp_name/version
    version_path = path / latest_ver_ptr->to_string();
    // root/comp_name/version/META.json
    path /= fs::path(latest_ver_ptr->to_string()) / Const::CONFIG_FILE_NAME;
  } else {
    //
    // 指定了版本，检查并得到版本对象实例
    //
    auto proper_ver_ptr = get_proper_version(comp_name, version, error);
    if (!proper_ver_ptr) {
      error = "Load component " + comp_name +
              " META.json failed [Version not satisfied " + version + "]";
      return nullptr;
    }
    // root / comp_name / version
    version_path = path / proper_ver_ptr->to_string();
    // root/comp_name/version/META.json
    path /= fs::path(proper_ver_ptr->to_string()) / Const::CONFIG_FILE_NAME;
  }
  if (!fs::exists(path)) {
    //
    // META.json文件不存在
    //
    error = "Load component " + comp_name + " META.json failed [" +
            path.string() + "]";
    return nullptr;
  }
  Json::Value root;
  auto ret = util::get_json_root_from_file(path.string(), root, error);
  if (!ret) {
    error = "Load component " + comp_name + " META.json failed [" + error + "]";
    return nullptr;
  }
  //
  // 获取入口DLL/SO
  //
  if (!root.isMember("entry_module")) {
    error = "Load component  " + comp_name +
            " META.json failed [entry_module not found]";
    return nullptr;
  }
  if (!root["entry_module"].isString()) {
    error = "Load component  " + comp_name +
            " META.json failed [entry_module MUST be a string]";
    return nullptr;
  }
  //
  // 是否是系统组件
  //
  auto is_system = false;
  //
  // 检查是否为系统组件
  //
  if (root.isMember("is_system")) {
    if (!root["is_system"].isBool()) {
      error = "Load component  " + comp_name +
              " META.json failed [is_system MUST be a boolean]";
      return nullptr;
    }
    is_system = root["is_system"].asBool();
  }
  // .dll/.so路径, root/comp_name/version/xx.so
  auto mod_path = version_path / root["entry_module"].asString();
  if (is_loaded(mod_path.string())) {
    //
    // 已经加载的组件
    //
    return comp_path_map_[mod_path.string()];
  }
  std::shared_ptr<ComponentImpl> ptr;
  if (is_system) {
    // 建立系统组件实例
    ptr = std::make_shared<SystemComponentImpl>();
    ptr->set_system(true);
  } else {
    // 建立组件实例
    ptr = std::make_shared<ComponentImpl>();
    ptr->set_system(false);
  }
  if (root.isMember("version")) {
    if (!root["version"].isString()) {
      error = "Load component  " + comp_name +
              " META.json failed [version is not string]";
      return nullptr;
    }
    //
    // 获取实际的version字段
    //
    auto ver_ptr = VersionImpl::from_string(root["version"].asString(), error);
    if (!ver_ptr) {
      error =
          "Load component  " + comp_name + " META.json failed [" + error + "]";
      return nullptr;
    }
    ptr->set_version(std::move(ver_ptr));
  }
  //
  // 获取descriptor
  //
  if (root.isMember("descriptor")) {
    auto desc_ptr = get_descriptor(root, error);
    if (!desc_ptr) {
      return nullptr;
    }
    ptr->set_descriptor(std::move(desc_ptr));
  }
  //
  // add dll/so search directory
  //
  util::set_current_object_search_path(version_path.string());
  //
  // 解决依赖关系
  //
  if (root.isMember("deps")) {
    if (!resolve_deps(root["deps"], error)) {
      return nullptr;
    }
  }
  //
  // 加载dll/so
  //
  if (!ptr->load(mod_path.string(), error)) {
    error = "Load component failed [" + mod_path.string() + ":" + error + "]";
    return nullptr;
  }
  if (ptr->is_system()) {
    //
    // 调用系统组件的on_init函数
    //
    auto sys_com_ptr = std::dynamic_pointer_cast<SystemComponent>(ptr);
    if (!sys_com_ptr->start(box_)) {
      error = "Load component failed [" + mod_path.string() + "::on_init()]";
      return nullptr;
    }
    if (!sys_com_ptr->on_init(box_)) {
      error = "Load component failed [" + mod_path.string() + ":on_init()]";
      return nullptr;
    }
  }
  // {root/comp_name/version/xx.so, ComponentPtr}
  comp_path_map_[mod_path.string()] = ptr;
  box_->write_log_line(klogger::Logger::INFORMATION,
                       "[box][component]Load " +
                           std::string(ptr->is_system() ? "SYSTEM " : "") +
                           "component " + mod_path.string() + " successfully");
  return ptr;
}

auto ComponentFactoryImpl::load_remote(const std::string &comp_name,
                                       const std::string &source,
                                       std::string &error,
                                       const std::string &version)
    -> ComponentPtr {
  //
  // 检查本地是否存在，存在则加载
  //
  if (get_proper_version(comp_name, version, error)) {
    return load(comp_name, error, version);
  }
  //
  // 下载组件
  //
  if (!download(comp_name, error, version, source)) {
    return nullptr;
  }
  //
  // 加载组件
  //
  return load(comp_name, error, version);
}

auto ComponentFactoryImpl::start(service::ServiceBox *box, std::string &error)
    -> bool {
  box_ = box;
  auto &config_ptr = box->get_config();
  if (!config_ptr.has_attribute("component")) {
    // 没有配置组件系统
    return true;
  }
  if (config_ptr.has_attribute("component.root_dir")) {
    try {
      // 设置组件根路径
      set_root_directory(config_ptr.get_string("component.root_dir"));
    } catch (...) {
      error = "component.root_dir MUST be a string";
      return false;
    }
  }
  return load_all(&config_ptr, error);
}

auto ComponentFactoryImpl::stop() -> bool {
  for (auto &[name, com] : comp_path_map_) {
    if (com->is_system()) {
      //
      // 系统组件
      //
      auto sys_com_ptr = std::dynamic_pointer_cast<SystemComponent>(com);
      if (!sys_com_ptr->on_destroy(box_)) {
        box_->write_log_line(klogger::Logger::FAILURE,
                             "[box][component]stop " + name + " failed");
      }
    }
  }
  return true;
}

auto ComponentFactoryImpl::update(std::time_t ts) -> void {
  for (auto &[name, com] : comp_path_map_) {
    if (com->is_system()) {
      //
      // 系统组件
      //
      auto sys_com_ptr = std::dynamic_pointer_cast<SystemComponent>(com);
      if (!sys_com_ptr->on_tick(box_, ts)) {
        box_->write_log_line(klogger::Logger::FAILURE,
                             "[box][component]update " + name + " failed");
      }
    }
  }
}

auto ComponentFactoryImpl::resolve_deps(const Json::Value &deps,
                                        std::string &error) -> bool {
  //
  // TODO 循环依赖处理
  //

  // "deps" : [
  //   {
  //     "version" : ">=1.0.1",
  //     "name" : "OtherComp",
  //     "source" : "http://xx.com/path"
  //   }
  // ]
  for (const auto &dep : deps) {
    //
    // 需求的版本号，不设置则为最新版本
    //
    std::string version;
    if (!dep.isMember("name")) {
      error = "Load component deps failed [need a name name]";
      return false;
    }
    if (!dep["name"].isString()) {
      error = "Load component deps failed [name MUST be a string]";
      return false;
    }
    if (dep.isMember("version")) {
      if (!dep["version"].isString()) {
        error = "Load component deps failed [version MUST be a string]";
        return false;
      }
      version = dep["version"].asString();
    }
    //
    // 获取本地对应版本信息, 如果本地不满足则下载
    //
    auto ver_ptr = get_version_info(dep["name"].asString(), version, error);
    if (!ver_ptr && !download(dep["name"].asString(), error, version,
                              dep["source"].asString())) {
      error = "Load component deps failed [" + error + "]";
      return false;
    }
    //
    // 加载组件
    //
    if (!load(dep["name"].asString(), error, version)) {
      error = "Load component deps failed [" + error + "]";
      return false;
    }
  }
  return true;
}

auto ComponentFactoryImpl::download(const std::string &comp_name,
                                    std::string &error,
                                    const std::string &version,
                                    const std::string &source) -> bool {
  namespace fs = std::filesystem;
  //
  // URL用于下载pack.json
  // eg.
  // source?com=xx&version=>1.0.0
  //
  auto url =
      source + "?com=" + comp_name + "&version=" + util::url_encode(version);
  service::ScopeCurl curl;
  auto ret = curl.perform(url);
  if (!ret) {
    error = "Download pack.json failed, CURL errno[" +
            std::to_string(curl.get_errno()) + "][" + url + "]";
    return false;
  }
  //
  // pack.json文件被下载到: root/comp_name/temp/pack.json
  //
  // pack.json:
  //
  // {
  //   "version" : "1.0.1",
  //   "files" : [
  //     {
  //       "name" : "file_name",
  //       "url" : "http://xx"
  //   ]
  // }
  //
  auto temp_dir = fs::path(root_dir_) / comp_name / "temp";
  if (!fs::exists(temp_dir.string())) {
    fs::create_directories(temp_dir);
  }
  // root/comp_name/temp/pack.json
  auto pack_json_path = temp_dir / "pack.json";
  std::ofstream ofs;
  ofs.open(pack_json_path.string(), std::ios::trunc | std::ios::out);
  if (!ofs) {
    error = "Internal error, cannot open [" + pack_json_path.string() + "]";
    return false;
  }
  curl.to_stream(ofs);
  Json::Value pack_json;
  if (!util::get_json_root_from_file(pack_json_path.string(), pack_json,
                                     error)) {
    error = "pack.json invalid [" + url + "]";
    return false;
  }
  if (!pack_json.isMember("version") || !pack_json["version"].isString()) {
    error = "pack.json invalid [" + url + "]";
    return false;
  }
  //
  // 建立版本目录
  // root/comp_name/version
  //
  auto root_dir =
      fs::path(root_dir_) / comp_name / pack_json["version"].asString();
  if (!fs::exists(root_dir)) {
    fs::create_directories(root_dir);
  }
  //
  // 将文件下载到root/comp_name/version
  //
  if (pack_json.isMember("files")) {
    for (const auto &file : pack_json["files"]) {
      service::ScopeCurl file_curl;
      auto file_name = file["name"].asString();
      auto file_url = file["url"].asString();
      //
      // 下载文件
      // url?name=xx
      //
      if (!file_curl.perform(file_url +
                             "?name=" + util::url_encode(file_name))) {
        error =
            "Download file failed [" + file_url + "?name=" + file_name + "]";
        return false;
      }
      //
      // 写入文件
      //
      std::ofstream file_ofs;
      file_ofs.open(root_dir / file_name,
                    std::ios::trunc | std::ios::out | std::ios::binary);
      if (!file_ofs) {
        error = "Internal error, cannot open [" +
                (root_dir / file_name).string() + "]";
        return false;
      }
      file_curl.to_stream(file_ofs);
    }
  }
  return true;
}

auto ComponentFactoryImpl::get_version_info(const std::string &comp_name,
                                            const std::string &version,
                                            std::string &error)
    -> VersionUniPtr {
  namespace fs = std::filesystem;
  //
  // 按照版本要求获取合适的版本
  //
  auto ver_ptr = get_proper_version(comp_name, version, error);
  if (!ver_ptr) {
    error = "Load component META.json failed [version not satisfied " +
            comp_name + ":" + version + "]";
    return nullptr;
  }
  //
  // 组件配置文件路径
  // root/comp_name/version/META.json
  //
  auto path = root_dir_ / fs::path(comp_name) / ver_ptr->to_string() /
              Const::CONFIG_FILE_NAME;
  if (!fs::exists(path)) {
    error = "Load component META.json failed [" + path.string() + "]";
    return nullptr;
  }
  Json::Value root;
  auto ret = util::get_json_root_from_file(path.string(), root, error);
  if (!ret) {
    error = "Load component META.json failed [" + error + "]";
    return nullptr;
  }
  //
  // 从配置文件内获取实际配置的版本号
  //
  std::string real_version;
  if (root.isMember("version")) {
    if (!get_version(root, real_version, error)) {
      error = "Load component META.json failed [" + path.string() + ":" +
              error + "]";
      return nullptr;
    }
    auto real_ver_ptr =
        VersionImpl::from_string(root["version"].asString(), error);
    // 版本号校验
    if (!real_ver_ptr || (*real_ver_ptr != *ver_ptr)) {
      error = "Load component META.json failed [" + path.string() + ":" +
              error + "]";
      return nullptr;
    }
    return real_ver_ptr;
  }
  return nullptr;
}

auto ComponentFactoryImpl::get_highest_version(const std::string &comp_name)
    -> VersionUniPtr {
  namespace fs = std::filesystem;
  std::vector<std::string> result;
  auto path = fs::path(root_dir_) / comp_name;
  util::get_file_name_in_directory(path.string(), "", result);
  VersionUniPtr ver_ptr;
  for (const auto &dir : result) {
    if (dir == "temp") {
      continue;
    }
    std::string error;
    if (!ver_ptr) {
      ver_ptr = VersionImpl::from_string(dir, error);
      if (!ver_ptr) {
        return nullptr;
      }
    } else {
      auto cur_ver_ptr = VersionImpl::from_string(dir, error);
      if (!cur_ver_ptr) {
        return nullptr;
      }
      if (*cur_ver_ptr > *ver_ptr) {
        ver_ptr = std::move(cur_ver_ptr);
      }
    }
  }
  return ver_ptr;
}

auto ComponentFactoryImpl::get_proper_version(const std::string &comp_name,
                                              const std::string &version,
                                              std::string &error)
    -> VersionUniPtr {
  namespace fs = std::filesystem;
  //
  // 版本
  // [比较符号]version
  // eg.
  // 1.0.1
  // >1.0.1
  // >=1.0.1
  // <1.0.1
  // <=1.0.1
  // !=1.0.1
  //

  //
  // 字符解释器状态
  //
  enum class State {
    NONE,
    EQUEL,
    LARGE,
    LESS,
    NOT,
    DIGIT,
    DOT,
  };
  //
  // 操作符
  //
  enum class Operate {
    NONE,
    LARGER,
    LESSER,
    LARGER_EQUAL,
    LESSER_EQUAL,
    NOT_EQUAL,
  };
  auto state{State::NONE};
  auto op{Operate::NONE};
  std::string value;
  //
  // 分析版本
  // 1. 操作符
  // 2. 版本号
  //
  for (const char c : version) {
    switch (c) {
    case '>':
      state = State::LARGE;
      op = Operate::LARGER;
      break;
    case '<':
      state = State::LESS;
      op = Operate::LESSER;
      break;
    case '!':
      state = State::NOT;
      break;
    case '=':
      if (state == State::LARGE) {
        op = Operate::LARGER_EQUAL;
      } else if (state == State::LESS) {
        op = Operate::LESSER_EQUAL;
      } else if (state == State::NOT) {
        op = Operate::NOT_EQUAL;
      } else {
        error = "Invalid version " + version;
        return nullptr;
      }
      break;
    default:
      value += c;
      break;
    }
  }
  if (op == Operate::NONE) {
    //
    // 没有操作符, 获取合适的版本
    //
    if (value.empty()) {
      // 没有指定版本，获取最新版本
      return get_highest_version(comp_name);
    } else {
      // 指定了版本，获取特定版本
      return get_version(comp_name, version, error);
    }
  }
  //
  // 目标版本
  //
  auto target_ver_ptr = VersionImpl::from_string(value, error);
  if (!target_ver_ptr) {
    error = "Invalid version " + version;
    return nullptr;
  }
  std::vector<std::string> result;
  auto path = fs::path(root_dir_) / comp_name;
  util::get_file_name_in_directory(path.string(), "", result);
  //
  // 应用操作符查找符合要求的版本号
  //
  for (const auto &dir : result) {
    if (dir == "temp") {
      continue;
    }
    auto ver_ptr = VersionImpl::from_string(dir, error);
    if (!ver_ptr) {
      return nullptr;
    }
    switch (op) {
    case Operate::LARGER:
      if (*ver_ptr > *target_ver_ptr) {
        return ver_ptr;
      }
      break;
    case Operate::LESSER:
      if (*ver_ptr < *target_ver_ptr) {
        return ver_ptr;
      }
      break;
    case Operate::LARGER_EQUAL:
      if (*ver_ptr >= *target_ver_ptr) {
        return ver_ptr;
      }
      break;
    case Operate::LESSER_EQUAL:
      if (*ver_ptr <= *target_ver_ptr) {
        return ver_ptr;
      }
      break;
    case Operate::NOT_EQUAL:
      if (*ver_ptr != *target_ver_ptr) {
        return ver_ptr;
      }
      break;
    default:
      error = "Invalid version [" + version + "]";
      return nullptr;
    }
  }
  return nullptr;
}

auto ComponentFactoryImpl::get_version(const Json::Value &v,
                                       std::string &version, std::string &error)
    -> bool {
  if (v.isMember("version")) {
    if (!v["version"].isString()) {
      error = "version MUST be a string";
      return false;
    }
    version = v["version"].asString();
    return true;
  }
  return false;
}

auto ComponentFactoryImpl::get_version(const std::string &comp_name,
                                       const std::string &version,
                                       std::string &error) -> VersionUniPtr {
  namespace fs = std::filesystem;
  std::vector<std::string> result;
  auto path = fs::path(root_dir_) / comp_name;
  util::get_file_name_in_directory(path.string(), "", result);
  VersionUniPtr ver_ptr;
  for (const auto &dir : result) {
    if (dir == version) {
      return VersionImpl::from_string(version, error);
    }
  }
  error = "version [" + version + "] not found";
  return nullptr;
}

auto ComponentFactoryImpl::is_loaded(const std::string &comp_path) -> bool {
  return (comp_path_map_.find(comp_path) != comp_path_map_.end());
}

auto ComponentFactoryImpl::get_descriptor(const Json::Value &root,
                                          std::string &error)
    -> DescriptorUniPtr {
  if (root.isMember("descriptor")) {
    if (!root["descriptor"].isObject()) {
      error = "Load component META.json failed [descriptor is not object]";
      return nullptr;
    }
    auto desc_ptr = std::make_unique<DescriptorImpl>();
    const auto &desc = root["descriptor"];
    for (const auto &k : desc.getMemberNames()) {
      desc_ptr->add_value(k, desc[k].asString());
    }
    return desc_ptr;
  }
  return nullptr;
}

auto ComponentFactoryImpl::load_all(config::BoxConfig *config_ptr,
                                    std::string &error) -> bool {
  auto auto_load = true;
  if (config_ptr->has_attribute("component.auto_load")) {
    try {
      auto_load = config_ptr->get_bool("component.auto_load");
    } catch (...) {
      error = "component.root_dir MUST be a boolean";
      return false;
    }
  }
  if (auto_load) {
    // 加载所有组件
    std::vector<std::string> result;
    util::get_file_name_in_directory(root_dir_, "", result);
    for (const auto &file : result) {
      if (!load(file, error)) {
        return false;
      }
    }
  }
  auto *zone = config_ptr->get_config_ptr()->zone("component");
  while (zone->hasNext()) {
    auto *attr = zone->next();
    if (attr->isZone()) {
      std::string version;
      auto version_attr_name = "component." + attr->name() + ".version";
      if (config_ptr->has_attribute(version_attr_name)) {
        try {
          version = config_ptr->get_string(version_attr_name);
        } catch (...) {
          error = version_attr_name + " MUST be a string";
          return false;
        }
      }
      std::string source;
      auto source_attr_name = "component." + attr->name() + ".source";
      try {
        source =
            config_ptr->get_string("component." + attr->name() + ".source");
      } catch (...) {
        error = source_attr_name + " MUST be a string";
        return false;
      }
      if (!load_remote(attr->name(), source, error, version)) {
        return false;
      }
    }
  }
  return true;
}

SystemComponentImpl::SystemComponentImpl() {}

SystemComponentImpl::~SystemComponentImpl() {}

auto SystemComponentImpl::start(service::ServiceBox *box) -> bool {
  init_func_ = get_func("on_init");
  destroy_func_ = get_func("on_destroy");
  tick_func_ = get_func("on_tick");
  return true;
}

auto SystemComponentImpl::stop(service::ServiceBox * /*box*/) -> bool {
  // 保留
  return true;
}

auto SystemComponentImpl::on_init(service::ServiceBox *box) -> bool {
  if (!init_func_) {
    return true;
  }
  using InitFunc = bool (*)(service::ServiceBox *);
  return ((InitFunc)init_func_)(box);
}

auto SystemComponentImpl::on_destroy(service::ServiceBox *box) -> bool {
  if (!destroy_func_) {
    return true;
  }
  using DestroyFunc = bool (*)(service::ServiceBox *);
  return ((DestroyFunc)destroy_func_)(box);
}

auto SystemComponentImpl::on_tick(service::ServiceBox *box, std::time_t ts)
    -> bool {
  if (!tick_func_) {
    return true;
  }
  using TickFunc = bool (*)(service::ServiceBox *, std::time_t);
  return ((TickFunc)tick_func_)(box, ts);
}

} // namespace component
} // namespace kratos

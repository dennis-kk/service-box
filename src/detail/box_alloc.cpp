#include "box_alloc.hh"

#include "box/fixed_mem_pool.hh"
#include <cstdlib>

char *kratos::service::box_malloc(std::size_t size) {
  return reinterpret_cast<char *>(malloc(size));
}

void kratos::service::box_free(void *p) {
  if (!p) {
    return;
  }
  return free(p);
}

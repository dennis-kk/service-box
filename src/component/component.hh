#pragma once

#include <cstdint>
#include <ctime>
#include <memory>
#include <string>
#include <stdexcept>

namespace kratos {
namespace service {
class ServiceContext;
class ServiceBox;
} // namespace service
} // namespace kratos

namespace kratos {
namespace component {

class Component;
class Version;
class Descriptor;

using ComponentPtr = std::shared_ptr<Component>;

/**
 * 组件工厂
 */
class ComponentFactory {
public:
  /**
   * 析构
   */
  virtual ~ComponentFactory() {}
  /**
   * 设置存放组件的目录
   * @param root_directory 存放组件的目录
   */
  virtual auto set_root_directory(const std::string &root_directory)
      -> void = 0;
  /**
   * 加载组件
   * @param comp_name 组件名
   * @param error 错误描述
   * @param version 版本描述, 默认将加载本地最新版本
   * @param entry_func 入口函数名
   * @return 组件实例
   */
  virtual auto load(const std::string &comp_name, std::string &error,
                    const std::string &version = "") -> ComponentPtr = 0;
  /**
   * 加载组件
   * @param comp_name 组件名
   * @param source 远程地址
   * @param error 错误描述
   * @param version 版本描述, 默认将加载本地最新版本
   * @return 组件实例
   */
  virtual auto load_remote(const std::string &comp_name,
                           const std::string &source, std::string &error,
                           const std::string &version = "") -> ComponentPtr = 0;
};

using VersionUniPtr = std::unique_ptr<Version>;

/**
 * 版本号
 */
class Version {
public:
  virtual ~Version() {}
  virtual auto to_string() -> const std::string & = 0;
  virtual auto major() const -> std::int32_t = 0;
  virtual auto minor() const -> std::int32_t = 0;
  virtual auto patch() const -> std::int32_t = 0;
  virtual auto operator>(const Version &rht) const -> bool = 0;
  virtual auto operator<(const Version &rht) const -> bool = 0;
  virtual auto operator==(const Version &rht) const -> bool = 0;
  virtual auto operator>=(const Version &rht) const -> bool = 0;
  virtual auto operator<=(const Version &rht) const -> bool = 0;
  virtual auto operator!=(const Version &rht) const -> bool = 0;
};

using DescriptorUniPtr = std::unique_ptr<Descriptor>;

/**
 * 用户数据描述
 */
class Descriptor {
public:
  virtual ~Descriptor() {}
  /**
   * 获取用户数据描述
   * @param key 名字
   * @return 值
   */
  virtual auto get_value(const std::string &key) const
      -> const std::string & = 0;
};

/**
 * 组件
 */
class Component {
public:
  /**
   * 析构
   */
  virtual ~Component() {}
  /**
   * 检测是否是系统组件
   *
   * \return true是, false否
   */
  virtual auto is_system() -> bool = 0;
  /**
   * 获取版本号
   */
  virtual auto get_version() -> const Version * = 0;
  /**
   * 获取用户数据描述
   */
  virtual auto get_descriptor() -> const Descriptor * = 0;
  /**
   * 获取组件导出函数地址
   * @param name 导出函数名
   * @return 类实例指针
   */
  virtual auto get_func(const std::string &name) -> void * = 0;
  /**
   * 调用组件内函数
   * @param name 函数名
   * @param args 参数
   * @return 返回值
   */
  template <typename T, typename... Args>
  auto call(const std::string &name, Args... args) -> T {
    using Func = T (*)(Args...);
    auto func_ptr = (Func)get_func(name);
    if (!func_ptr) {
      throw std::runtime_error("Function not found [" + name + "]");
    }
    return func_ptr(std::forward<Args>(args)...);
  }
};

/**
 * 调用C函数工具类
 */
template <typename F> struct CFunction;
template <typename R, typename... Args> struct CFunction<R(Args...)> {
  using cfunction_type = R (*)(Args...);
  /**
   * 调用C函数
   * @param cfunc_addr C函数地址
   * @param args 函数参数
   */
  inline static auto call(void *cfunc_addr, Args... args) -> R {
    if (!cfunc_addr) {
      throw std::runtime_error("Function not found");
    }
    return ((cfunction_type)cfunc_addr)(std::forward<Args>(args)...);
  }
};

/**
 * 系统组件
 */
class SystemComponent {
public:
  virtual ~SystemComponent() {}
  /**
   * 启动
   * @param box 容器指针
   * @return true成功, false失败
   */
  virtual auto start(service::ServiceBox *box) -> bool = 0;
  /**
   * 关闭
   * @param box 容器指针
   * @return true成功, false失败
   */
  virtual auto stop(service::ServiceBox *box) -> bool = 0;
  /**
   * 当需要初始化组件时调用
   * @param box 容器指针
   * @return true成功, false失败
   */
  virtual auto on_init(service::ServiceBox *box) -> bool = 0;
  /**
   * 当需要销毁组件时调用
   * @param box 容器指针
   * @return true成功, false失败
   */
  virtual auto on_destroy(service::ServiceBox *box) -> bool = 0;
  /**
   * 每帧调用
   * @param box 容器指针
   * @param ts 当前时间戳(毫秒)
   * @return true成功, false失败
   */
  virtual auto on_tick(service::ServiceBox *box, std::time_t ts) -> bool = 0;
};

} // namespace component
} // namespace kratos

﻿#ifndef JSON_PB_MODULE_H
#define JSON_PB_MODULE_H

#if !defined(__linux__) && !defined(__APPLE__)
#ifdef __cplusplus
#define FuncExport extern "C" __declspec(dllexport) // WIN32 DLL exporter
#else
#define FuncExport __declspec(dllexport) // WIN32 DLL exporter
#endif
#else
#ifdef __cplusplus
#define FuncExport extern "C" // LINUX shared object exporter
#else
#define FuncExport
#endif
#endif                       // !defined(__linux__)

/**
 * 建立对象并初始化
 * 
 * \return 对象ID, 大于0.
 */
FuncExport int json_pb_init();
/**
 * 设置.json和.proto文件的根目录, 支持子目录
 * 
 * \param jpb 对象ID
 * \param path 目录路径
 * \retval 0 成功
 * \retval 非0 失败, json_pb_get_error_string可获取错误描述.
 */
FuncExport int json_pb_set_directory(int jpb, const char *path);
/**
 * 将JSON字符串转化为Protobuf字节流.
 * 
 * \param jpb 对象ID
 * \param service_name 服务名
 * \param method_name 方法名
 * \param json_args JSON字符串, 方法参数
 * \retval 0 失败, json_pb_get_error_string可获取错误描述.
 * \retval 字节流地址
 */
FuncExport const char *json_pb_to_pb_bytes(int jpb,
                                                const char *service_name,
                                                const char *method_name,
                                                const char *json_args);
/**
 * 获取上一次调用json_pb_to_pb_bytes返回的字节流长度.
 * 
 * \param jpb 对象ID
 * \retval -1 失败, json_pb_get_error_string可获取错误描述.
 * \retval 字节流长度
 */
FuncExport int json_pb_get_pb_bytes_length(int jpb);
/**
 * 将Protobuf字节流转换为JSON字符串.
 * 
 * \param jpb 对象ID
 * \param service_name  服务名
 * \param method_name 方法名
 * \param pb_bytes Protobuf字节流
 * \param length 字节流长度
 * \return 0 失败, json_pb_get_error_string可获取错误描述.
 * \return JSON字符串
 */
FuncExport const char *json_pb_to_json_string(int jpb,
                                                   const char *service_name,
                                                   const char *method_name,
                                                   const char *pb_bytes,
                                                   int length);
/**
 * 获取上一次的错误描述.
 * 
 * \param jpb 对象ID
 * \return 错误描述
 */
FuncExport const char *json_pb_get_error_string(int jpb);
/**
 * 销毁对象
 * 
 * \param jpb 对象ID
 * \retval -1 失败, json_pb_get_error_string可获取错误描述.
 * \retval 0 成功
 */
FuncExport int json_pb_deinit(int jpb);

#endif /* JSON_PB_MODULE_H */

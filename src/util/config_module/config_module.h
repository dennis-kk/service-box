﻿#ifndef CONFIG_MODULE_H
#define CONFIG_MODULE_H

#ifdef WIN32
#ifdef __cplusplus
#define DLL_EXPORT_C_DECL extern "C" __declspec(dllexport)
#define DLL_IMPORT_C_DECL extern "C" __declspec(dllimport)
#define DLL_EXPORT_DECL extern __declspec(dllexport)
#define DLL_IMPORT_DECL extern __declspec(dllimport)
#define DLL_EXPORT_CLASS_DECL __declspec(dllexport)
#define DLL_IMPORT_CLASS_DECL __declspec(dllimport)
#else
#define DLL_EXPORT_DECL __declspec(dllexport)
#define DLL_IMPORT_DECL __declspec(dllimport)
#endif
#else
#ifdef __cplusplus
#define DLL_EXPORT_C_DECL extern "C"
#define DLL_IMPORT_C_DECL extern "C"
#define DLL_EXPORT_DECL extern
#define DLL_IMPORT_DECL extern
#define DLL_EXPORT_CLASS_DECL
#define DLL_IMPORT_CLASS_DECL
#else
#define DLL_EXPORT_DECL extern
#define DLL_IMPORT_DECL extern
#endif
#endif

/**
 * 加载配置
 * 
 * \param path 配置文件路径.
 * \return 0 成功, 非0失败
 */
DLL_EXPORT_DECL int kconfig_load(const char* path);
/**
 * 关闭并清理配置器.
 */
DLL_EXPORT_DECL void kconfig_close();
/**
 * 获取最后一个错误描述.
 * 
 * \return 错误描述
 */
DLL_EXPORT_DECL const char* kconfig_get_last_error();
/**
 * 获取最后一个错误码.
 *
 * \return 错误码
 */
DLL_EXPORT_DECL int kconfig_get_last_errno();
/**
 * 根据错误码获取错误描述
 * 
 * \param error 错误码.
 * \return 错误描述
 */
DLL_EXPORT_DECL const char* kconfig_get_error(int error);
/**
 * 获取bool类型配置.
 * 
 * \param key 配置名
 * \return 0 false, 1 true
 */
DLL_EXPORT_DECL int kconfig_get_bool(const char* key);
/**
 * 获取string类型配置.
 *
 * \param key 配置名
 * \retval NULL失败
 * \retval 配置值
 */
DLL_EXPORT_DECL const char* kconfig_get_string(const char* key);
/**
 * 获取int类型配置.
 *
 * \param key 配置名
 * \retval 配置值, 调用后使用kconfig_get_last_errno可检查是否成功
 */
DLL_EXPORT_DECL int kconfig_get_int(const char* key);
/**
 * 获取unsigned int类型配置.
 *
 * \param key 配置名
 * \retval 配置值, 调用后使用kconfig_get_last_errno可检查是否成功
 */
DLL_EXPORT_DECL unsigned int kconfig_get_uint(const char* key);
/**
 * 获取long long类型配置.
 *
 * \param key 配置名
 * \retval 配置值, 调用后使用kconfig_get_last_errno可检查是否成功
 */
DLL_EXPORT_DECL long long kconfig_get_llong(const char* key);
/**
 * 获取unsigned long long类型配置.
 *
 * \param key 配置名
 * \retval 配置值, 调用后使用kconfig_get_last_errno可检查是否成功
 */
DLL_EXPORT_DECL unsigned long long kconfig_get_ullong(const char* key);
/**
 * 获取float类型配置.
 *
 * \param key 配置名
 * \retval 配置值, 调用后使用kconfig_get_last_errno可检查是否成功
 */
DLL_EXPORT_DECL float kconfig_get_float(const char* key);
/**
 * 获取容器类型配置(数组, 字典, 域).
 *
 * \param key 配置名, key为NULL则返回整个配置文件的JSON串
 * \retval 返回配置值的JSON串
 */
DLL_EXPORT_DECL const char* kconfig_get_json(const char* key);

#endif /* CONFIG_MODULE_H */

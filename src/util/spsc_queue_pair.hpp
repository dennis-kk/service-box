#pragma once

#include "spsc_queue.hpp"
#include <memory>

namespace kratos {
namespace corelib {

/**
 * 使用两个一读一写无锁队列来模拟双向队列.
 */
template <typename T> class SPSCQueuePair {
  std::shared_ptr<SPSCQueue<T>> other_{nullptr}; ///< 写入队列
  std::shared_ptr<SPSCQueue<T>> my_{nullptr};    ///< 读取队列

public:
  /**
   * 构造
   * \param other 写入队列
   * \param my 读取队列
   */
  SPSCQueuePair(std::shared_ptr<SPSCQueue<T>> my,
                std::shared_ptr<SPSCQueue<T>> other) {
    other_ = my;
    my_ = other;
  }
  /**
   * 析构
   */
  ~SPSCQueuePair() {}
  /**
   * 写入
   * \param element 数据
   * \retval true 成功
   * \retval false 失败
   */
  bool send(T const &element) { return other_->enqueue(element); }
  /**
   * 写入
   * \param element 数据
   * \retval true 成功
   * \retval false 失败
   */
  bool send(T &&element) { return other_->enqueue(element); }
  /**
   * 读取
   * \param element 读取的数据
   * \retval true 成功
   * \retval false 失败
   */
  bool try_read(T &element) { return my_->try_dequeue(element); }
};

} // namespace corelib
} // namespace kratos

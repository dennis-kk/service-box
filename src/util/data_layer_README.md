# 数据层设计

数据层由两大部分组成:
1. SDK

   提供不同语言访问数据层的代码库

2. 数据服务

   提供数据访问的通用服务方法

## 数据服务的IDL定义

```
service DataService {
    bool save(string source)
    string load(string source)
    ui32 remove(string source)
}
```

`source`为JSON串, 格式文档后续会进行说明. 方法说明如下:

1. bool save(string source)

保存数据对象, source为JSON串, 返回`true`或`false`

2. string load(string source)

加载数据对象, source为JSON串, 返回JSON串

3. ui32 remove(string source)

删除数据对象, source为JSON串, 返回删除的个数

# 数据JSON API定义

## 字段类型

| 类型名  |  说明  |
|---|---|
| int  | 32位有符号整数  |
| uint   | 32位无符号整数  |
| int64   | 64位有符号整数   |
| uint64   | 64位无符号整数  |
| string  | 字符串  |
| float | 32位浮点数 |
| double | 64位浮点数 |

## 保存数据对象请求

DataService.save参数.

```json
{
    "cmd" : "save",
    "type": "对象类型名",
    "index_name" : "索引名",
    "index_type" : "索引类型",
    "index_value" : "索引值字符串",
    "values" : [
         {
             "name" : "字段名",
             "value" : "字段值字符串",
             "type" : "字段类型"
         },
         {
             ...
         }
    ]
}
```

## 新建数据对象请求

DataService.save参数.

```json
{
    "cmd" : "new",
    "type": "对象类型名",
    "index_name" : "索引名",
    "index_type" : "索引类型",
    "index_value" : "索引值字符串",
    "values" : [
         {
             "name" : "字段名",
             "value" : "字段值字符串",
             "type" : "字段类型"
         },
         {
             ...
         }
    ]
}
```

## 加载指定数据对象请求

DataService.load参数.

```json
{
    "cmd" : "load_index",
    "type": "对象类型名",
    "index_name" : "索引名",
    "index_type" : "索引类型",
    "index_value" : "索引值字符串",
}
```

## 加载指定数据对象返回

DataService.load返回.

```json
{
    "values" : [
         {
               "name" : "字段名[可选]",
               "value" : "字段值字符串"
         },
         {
               ...
         }
     ]
}
```

## 加载多个数据对象请求

DataService.load参数.

```json
{
    "cmd" : "load_multi_index",
    "type": "对象类型名",
    "index_name" : "索引名",
    "index_type" : "索引类型",
    "multi_index_value" : [
        "索引值字符串",
        ...
    ]
}
```

## 加载多个数据对象返回

DataService.load返回.

```json
{
    "multi_values" : [
        {
            "values" : [
                 {
                     "name" : "字段名[可选]",
                     "value" : "字段值字符串"
                 },
                 {
                     ...
                 }
            ]
        },
        ...
    ]
}
```

## 按照加载条件加载数据对象请求

DataService.load参数.

```json
{
     "cmd" : "load_all",
     "type": "对象类型名",
     "filter" : {
         ...
     }
}
```

`filter`字段为加载条件, 过滤器构成后续会说明.

## 按照加载条件加载数据对象返回

DataService.load返回.

```json
[
    {
        "values" : [
            {
               "name" : "字段名[可选]",
               "value" : "字段值字符串"
            },
            {
               ...
            }
         ]
     },
     {
         ...
     }
]
```

返回为JSON数组, 数组每一个元素为一个数据对象.

## 从数据表删除指定对象请求

DataService.remove参数.

```JSON
{
     "cmd" : "remove",
     "type": "对象类型名",
     "index_name" : "索引名",
     "index_type" : "索引类型",
     "index_value" : "索引值字符串",
}
```

## 按照条件从数据表删除对象请求

DataService.remove参数.

```json
{
     "cmd" : "remove",
     "type": "对象类型名",
     "filter" : {
         ...
     }
}
```
`filter`字段为删除条件, 过滤器构成后续会说明.


# 过滤器

TODO

# SDK

TODO
#pragma once

#include <initializer_list>
#include <string>
#include <string_view>
#include <tuple>
#include <type_traits>
#include <vector>

namespace kratos {
namespace util {

/**
 * 逐个对参数调用函数f.
 *
 * \param t 参数
 * \param f 函数
 * \param
 * \return
 */
template <class T, class F, std::size_t... I>
inline constexpr F foreach_impl(T &&t, F &&f, std::index_sequence<I...>) {
  return (void)std::initializer_list<int>{
             (std::forward<F>(f)(std::get<I>(std::forward<T>(t))), 0)...},
         f;
}
/**
 * 逐个对参数调用函数f.
 *
 * \param t 参数
 * \param f 函数
 * \param
 * \return
 */
template <class T, class F, std::size_t... I>
inline constexpr F foreach_impl_ref(T &t, F &&f, std::index_sequence<I...>) {
  return (void)std::initializer_list<int>{
             (std::forward<F>(f)(std::get<I>(t)), 0)...},
         f;
}
/**
 * 对容器内所有元素调用f, 参数为容器元素
 */
template <class T, class F> inline constexpr F tuple_foreach(T &&t, F &&f) {
  return foreach_impl(
      std::forward<T>(t), std::forward<F>(f),
      std::make_index_sequence<
          std::tuple_size<std::remove_reference_t<T>>::value>{});
}
/**
 * 对容器内所有元素调用f, 参数为容器元素
 */
template <class T, class F> inline constexpr F tuple_foreach_ref(T &t, F &&f) {
  return foreach_impl_ref(
      t, std::forward<F>(f),
      std::make_index_sequence<
          std::tuple_size<std::remove_reference_t<T>>::value>{});
}

} // namespace util
} // namespace kratos

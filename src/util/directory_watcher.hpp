﻿#pragma once

#include <chrono>
#include <filesystem>
#include <functional>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>

namespace kratos {
namespace util {

/**
 * 文件状态.
 */
enum class FileStatus : int { NONE, NEW, MODIFIED, DELETED };

/**
 * 变化事件.
 */
struct WatcherEvent {
  std::string file_path;                    ///< 文件路径
  FileStatus file_status{FileStatus::NONE}; ///< 文件状态
};

/**
 * 获取监控事件名
 *
 * \param status 事件ID
 */
inline static auto get_watch_event_name(FileStatus status) -> const char * {
  const char *name_array[] = {nullptr, "NEW", "MODIFIED", "DELETED"};
  return name_array[std::underlying_type<FileStatus>::type(status)];
}

using WatcherEventVector = std::vector<WatcherEvent>;

/**
 * 目录文件状态监控.
 */
class DirectoryWatcher {
public:
  /**
   * 构造.
   *
   * \param dir_to_watching 需要监控的目录
   */
  DirectoryWatcher(const std::string &dir_to_watching) {
    namespace fs = std::filesystem;
    path_to_watching_ = dir_to_watching;
    for (auto &file : fs::recursive_directory_iterator(path_to_watching_)) {
      path_map_[file.path().string()] = fs::last_write_time(file);
    }
  }
  /**
   * 检查目录变化.
   *
   * \param events 事件数组
   */
  auto watch(WatcherEventVector &events) -> void {
    namespace fs = std::filesystem;
    for (auto it = path_map_.begin(); it != path_map_.end();) {
      if (!fs::exists(it->first)) {
        events.emplace_back(WatcherEvent{it->first, FileStatus::DELETED});
        it = path_map_.erase(it);
      } else {
        it++;
      }
    }
    /**
     * 检查目录内文件是否被修改.
     */
    for (auto &file : fs::recursive_directory_iterator(path_to_watching_)) {
      auto last_mod_time = fs::last_write_time(file);
      auto file_path = file.path().string();
      if (!exists(file_path)) {
        // 新建
        path_map_[file_path] = last_mod_time;
        events.emplace_back(WatcherEvent{file_path, FileStatus::NEW});
      } else {
        // 修改
        auto &mod_time_ref = path_map_[file_path];
        if (mod_time_ref != last_mod_time) {
          mod_time_ref = last_mod_time;
          events.emplace_back(WatcherEvent{file_path, FileStatus::MODIFIED});
        }
      }
    }
  }

private:
  /**
   * 检查文件是否存在.
   *
   * \param file_path 文件路径
   * \return
   */
  auto exists(const std::string &file_path) -> bool {
    return (path_map_.find(file_path) != path_map_.end());
  }

private:
  using FileTimeMap =
      std::unordered_map<std::string, std::filesystem::file_time_type>;
  FileTimeMap path_map_;         ///< {文件路径, 修改时间}
  std::string path_to_watching_; ///< 需要监控的目录
};

} // namespace util
} // namespace kratos

﻿#pragma once

#include "box/service_context.hh"

#include <functional>
#include <memory>
#include <type_traits>
#include <unordered_map>

namespace kratos {
namespace util {

/**
 * Proxy Group.
 *
 * ProxyGroup会管理ProxyTypeT类型的服务代理,
 * 通过用户给出KeyT类型的键来维护一个快查表, 并维持引用.
 */
template <typename KeyT, typename ProxyTypeT> class ProxyGroup {
  kratos::service::ServiceContext *ctx_{nullptr}; ///< Service Context
  /**
   * Proxy Info.
   */
  struct ProxyInfo {
    std::shared_ptr<ProxyTypeT> prx_ptr;             ///< Proxy pointer
    std::function<void(decltype(prx_ptr), bool)> cb; ///< Callback
    kratos::service::CallbackAutoDeleterPtr
        auto_deleter; ///< Callback auto deleter
  };
  using ProxyMap = std::unordered_map<KeyT, std::shared_ptr<ProxyInfo>>;
  ProxyMap proxy_map_; ///< {user key, ProxyInfo pointer}
  using CbMap = std::unordered_map<std::uint32_t, std::shared_ptr<ProxyInfo>>;
  CbMap cb_map_; ///< {cb ID, ProxyInfo pointer}

public:
  //
  // typename for STL container
  //

  using iterator = typename ProxyMap::iterator;
  using size_type = typename ProxyMap::size_type;
  using const_iterator = typename ProxyMap::const_iterator;

  //
  // Proxy types
  //

  using ProxyCallback = std::function<void(std::shared_ptr<ProxyTypeT>, bool)>;
  using ProxyPtr = std::shared_ptr<ProxyTypeT>;

public:
  /**
   * ctor.
   *
   * \param ctx ServiceContext pointer
   */
  ProxyGroup(kratos::service::ServiceContext *ctx) { ctx_ = ctx; }
  /**
   * dtor.
   */
  ~ProxyGroup() {}
  /**
   * Query remote service and call user callback when query operation finishing.
   *
   * \param key user key
   * \param cb uesr callback
   */
  auto query(const KeyT &key, ProxyCallback cb) -> void {
    if (!ctx_) {
      return;
    }
    auto info_ptr = std::make_shared<ProxyInfo>();
    info_ptr->cb = cb;
    info_ptr->prx_ptr =
        rpc::getService<ProxyTypeT>(nullptr, true, ctx_->get_rpc());
    ctx_->get_proxy_helper<ProxyTypeT>(
        info_ptr->auto_deleter,
        std::bind(&ProxyGroup::_cb, this, std::placeholders::_1,
                  std::placeholders::_2, std::placeholders::_3));
    proxy_map_.emplace(key, info_ptr);
    if (info_ptr->auto_deleter) {
      cb_map_.emplace(info_ptr->auto_deleter->get_cb_id(), info_ptr);
    }
  }
  /**
   * Query remote service and call user callback when query operation finishing.
   *
   * \param key user key
   * \param service_name user given service name
   * \param cb uesr callback
   */
  auto query(const KeyT &key, const std::string &service_name, ProxyCallback cb)
      -> void {
    if (!ctx_) {
      return;
    }
    auto info_ptr = std::make_shared<ProxyInfo>();
    info_ptr->cb = cb;
    info_ptr->prx_ptr =
        rpc::getService<ProxyTypeT>(nullptr, true, ctx_->get_rpc());
    ctx_->get_named_proxy_helper<ProxyTypeT>(
        service_name, info_ptr->auto_deleter,
        std::bind(&ProxyGroup::_cb, this, std::placeholders::_1,
                  std::placeholders::_2, std::placeholders::_3));
    proxy_map_.emplace(key, info_ptr);
    if (info_ptr->auto_deleter) {
      cb_map_.emplace(info_ptr->auto_deleter->get_cb_id(), info_ptr);
    }
  }
  /**
   * Query remote service invoking in coroutine and wait given timeout at most.
   *
   * \param key user key
   * \param timeout waiting timeout(millionsecond)
   * \return service proxy or empty pointer
   */
  auto query_co(const KeyT &key, std::time_t timeout) -> ProxyPtr {
    if (!ctx_) {
      return nullptr;
    }
    auto prx_ptr = ctx_->get_proxy_co<ProxyTypeT>(timeout);
    if (!prx_ptr) {
      return nullptr;
    }
    auto info_ptr = std::make_shared<ProxyInfo>();
    info_ptr->prx_ptr = prx_ptr;
    proxy_map_.emplace(key, info_ptr);
    return prx_ptr;
  }
  /**
   * Query remote service invoking in coroutine and wait given timeout at most.
   *
   * \param key user key
   * \param service_name user given service name
   * \param timeout waiting timeout(millionsecond)
   * \return service proxy or empty pointer
   */
  auto query_co(const KeyT &key, const std::string &service_name,
                std::time_t timeout) -> ProxyPtr {
    if (!ctx_) {
      return nullptr;
    }
    auto prx_ptr = ctx_->get_proxy_co<ProxyTypeT>(service_name, timeout);
    if (!prx_ptr) {
      return nullptr;
    }
    auto info_ptr = std::make_shared<ProxyInfo>();
    info_ptr->prx_ptr = prx_ptr;
    proxy_map_.emplace(key, info_ptr);
    return prx_ptr;
  }
  /**
   * Get proxy instance by user key.
   *
   * \param key user key
   * \return service proxy or empty pointer
   */
  auto get(const KeyT &key) -> ProxyPtr {
    if (!ctx_) {
      return nullptr;
    }
    auto it = proxy_map_.find(key);
    if (it == proxy_map_.end()) {
      return nullptr;
    }
    return it->second->prx_ptr;
  }
  /**
   * Remove proxy instance by user key.
   */
  auto remove(const KeyT &key) -> void {
    if (!ctx_) {
      return;
    }
    auto it = proxy_map_.find(key);
    if (it == proxy_map_.end()) {
      return;
    }
    if (it->second->auto_deleter) {
      cb_map_.erase(it->second->auto_deleter->get_cb_id());
    }
    proxy_map_.erase(it);
  }

  //
  // method for STL container
  //

  iterator begin() noexcept { return proxy_map_.begin(); }
  const_iterator begin() const noexcept { return proxy_map_.begin(); }
  iterator end() noexcept { return proxy_map_.end(); }
  const_iterator end() const noexcept { return proxy_map_.end(); }
  const_iterator cbegin() const noexcept { return begin(); }
  const_iterator cend() const noexcept { return end(); }
  size_type size() const noexcept { return proxy_map_.size(); }
  bool empty() const noexcept { return proxy_map_.empty(); }
  iterator erase(iterator it) {
    if (it->second->auto_deleter) {
      cb_map_.erase(it->second.auto_deleter->get_cb_id());
    }
    return proxy_map_.erase(it);
  }
  iterator erase(const_iterator cit) {
    if (cit->second->auto_deleter) {
      cb_map_.erase(cit->second.auto_deleter->get_cb_id());
    }
    return proxy_map_.erase(cit);
  }
  void erase(const KeyT &key) { remove(key); }

private:
  /**
   * Query callback for framework.
   */
  auto _cb(kratos::service::TransEvent evt, std::uint32_t cb_id,
           std::shared_ptr<rpc::Transport> trans_ptr) -> void {
    auto cb_it = cb_map_.find(cb_id);
    if (cb_it == cb_map_.end()) {
      return;
    }
    if (evt == kratos::service::TransEvent::CLOSE) {
      if (cb_it->second->cb) {
        try {
          cb_it->second->cb(cb_it->second->prx_ptr, false);
        } catch (std::exception &e) {
          ctx_->write_log_line(klogger::Logger::EXCEPTION,
                               "Proxy[" + std::to_string(ProxyTypeT::uuid()) +
                                   "] Query callback exception, resason[" +
                                   e.what() + "]");
        }
      }
      return;
    }
    if (!trans_ptr) {
      return;
    }
    if (cb_it->second->auto_deleter &&
        (cb_it->second->auto_deleter->get_cb_id() != cb_id)) {
      return;
    }
    if (cb_it->second->prx_ptr &&
        cb_it->second->prx_ptr->getID() != rpc::INVALID_PROXY_ID) {
      cb_it->second->prx_ptr->setTransport(trans_ptr);
    } else {
      auto prx = rpc::getService<ProxyTypeT>(trans_ptr, true, ctx_->get_rpc());
      prx->setRpc(ctx_->get_rpc());
      prx->set_service_name(std::to_string(ProxyTypeT::uuid()));
      cb_it->second->prx_ptr->swap(prx);
    }
    if (cb_it->second->prx_ptr && cb_it->second->prx_ptr->isConnected()) {
      if (cb_it->second->cb) {
        try {
          cb_it->second->cb(cb_it->second->prx_ptr, true);
        } catch (std::exception &e) {
          ctx_->write_log_line(klogger::Logger::EXCEPTION,
                               "Proxy[" + std::to_string(ProxyTypeT::uuid()) +
                                   "] Query callback exception, resason[" +
                                   e.what() + "]");
        }
      }
    } else {
      ctx_->write_log_line(klogger::Logger::WARNING,
                           "Proxy UUID[" + std::to_string(ProxyTypeT::uuid()) +
                               "] not found in RPC core");
    }
  }
};

/**
 * ProxyGroup获取函数.
 *
 * \param ctx 第一次调用需要提供ServiceContext进行初始化,
 * 后续调用不需要提供ServiceContext \return ProxyGroup指针,
 * 未初始化则返回nullptr
 */
template <typename KeyT, typename ProxyTypeT>
inline static ProxyGroup<KeyT, ProxyTypeT> *
get_group(kratos::service::ServiceContext *ctx = nullptr) {
  static bool init{false};
  static std::unique_ptr<ProxyGroup<KeyT, ProxyTypeT>> group_ptr;
  if (!init) {
    if (!ctx) {
      return nullptr;
    }
    group_ptr = std::make_unique<ProxyGroup<KeyT, ProxyTypeT>>(ctx);
    init = true;
  }
  return group_ptr.get();
}

} // namespace util
} // namespace kratos

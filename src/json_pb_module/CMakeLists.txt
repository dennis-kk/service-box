cmake_minimum_required(VERSION 3.10)
set(CMAKE_CXX_STANDARD 17)
project (json_pb)
SET(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin/)

IF (MSVC)
    foreach(var
        CMAKE_C_FLAGS CMAKE_C_FLAGS_DEBUG CMAKE_C_FLAGS_RELEASE
        CMAKE_C_FLAGS_MINSIZEREL CMAKE_C_FLAGS_RELWITHDEBINFO
        CMAKE_CXX_FLAGS CMAKE_CXX_FLAGS_DEBUG CMAKE_CXX_FLAGS_RELEASE
        CMAKE_CXX_FLAGS_MINSIZEREL CMAKE_CXX_FLAGS_RELWITHDEBINFO
        )
        if(${var} MATCHES "/MD")
            string(REGEX REPLACE "/MD" "/MT" ${var} "${${var}}")
        endif()
    endforeach()
ELSE()
    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
        SET(CMAKE_CXX_FLAGS "-m64 -g -O2 -std=c++17 -Wall -Wno-parentheses -Wno-deprecated-declarations -fnon-call-exceptions -Wno-unused-command-line-argument -Wno-unknown-warning-option -fPIC")
        set(CMAKE_CXX_COMPILER "clang++")
    elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
        SET(CMAKE_CXX_FLAGS "-m64 -g -O2 -std=c++17 -Wall -Wno-parentheses -Wno-deprecated-declarations -fnon-call-exceptions -fPIC")
        set(CMAKE_CXX_COMPILER "g++")
    endif()
    find_program(CCACHE_PROGRAM ccache)
    if (CCACHE_PROGRAM)
        set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "${CCACHE_PROGRAM}")
    endif ()
ENDIF ()

aux_source_directory(${PROJECT_SOURCE_DIR}/../util/json_pb_module SRC_LIST)
aux_source_directory(${PROJECT_SOURCE_DIR}/../../thirdparty/src/jsoncpp SRC_LIST)

add_library(json_pb SHARED ${SRC_LIST})

message(${SRC_LIST})

#cross platfom file 
target_sources(json_pb PRIVATE
	${PROJECT_SOURCE_DIR}/../util/method_type.cpp
  	${PROJECT_SOURCE_DIR}/../util/string_util.cpp
    ${PROJECT_SOURCE_DIR}/../util/os_util.cpp
    ${PROJECT_SOURCE_DIR}/../util/write_buffer.cpp
    ${PROJECT_SOURCE_DIR}/../util/object_pool.cpp
    ${PROJECT_SOURCE_DIR}/../detail/box_alloc.cpp
)

IF (MSVC)
    target_link_libraries(json_pb PUBLIC
		debug ${PROJECT_SOURCE_DIR}/../../thirdparty/lib/win/debug/libprotobufd.lib optimized ${PROJECT_SOURCE_DIR}/../../thirdparty/lib/win/release/libprotobuf.lib
        ws2_32.lib
	)
    set_target_properties(json_pb PROPERTIES LINK_FLAGS "/ignore:4099")
else()
    link_directories(${PROJECT_SOURCE_DIR}/../thirdparty/lib/linux)
	target_link_libraries(json_pb PUBLIC
		${PROJECT_SOURCE_DIR}/../../thirdparty/lib/linux/libprotobuf.a
        -lpthread
	)
endif()

#设置包含路径以及安装时候的路径
target_include_directories(json_pb
	PUBLIC
	${PROJECT_SOURCE_DIR}
	${PROJECT_SOURCE_DIR}/..
	${PROJECT_SOURCE_DIR}/../../thirdparty 
	${PROJECT_SOURCE_DIR}/../../thirdparty/include 
)

target_compile_definitions(json_pb PUBLIC
    DISABLE_TRACE
    $<$<AND:$<PLATFORM_ID:Windows>,$<CONFIG:Debug>>: _WINSOCK_DEPRECATED_NO_WARNINGS WIN32 _WINDOWS _DEBUG _CONSOLE _CRT_SECURE_NO_WARNINGS WIN32_LEAN_AND_MEAN>
    $<$<AND:$<PLATFORM_ID:Windows>,$<CONFIG:Release>>: _WINSOCK_DEPRECATED_NO_WARNINGS _CRT_SECURE_NO_WARNINGS WIN32_LEAN_AND_MEAN WIN32 _WINDOWS NDEBUG>
)

target_compile_options(json_pb PUBLIC 
	$<$<PLATFORM_ID:Windows>: /wd4706 /wd4206 /wd4100 /wd4244 /wd4127 /wd4702 /wd4324 /wd4310 /wd4820 /W4 /MP /EHa>
)

IF (MSVC)
SET_TARGET_PROPERTIES(json_pb PROPERTIES 
                    PREFIX ""
                    DEBUG_POSTFIX "_d"
                    SUFFIX ".dll")
endif()
                    
target_compile_definitions(json_pb PUBLIC
$<$<CONFIG:Debug>: -DDEBUG -D_DEBUG>
$<$<CONFIG:Release>: -DRELEASE>
)

install(TARGETS json_pb
	LIBRARY DESTINATION bin
)

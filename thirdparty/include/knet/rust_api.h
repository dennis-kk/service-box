typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef signed short int16_t;
typedef unsigned int uint32_t;
typedef signed int int32_t;
typedef unsigned long long uint64_t ;
typedef signed long long int64_t;

typedef struct _loop_t kloop_t;
typedef struct _channel_ref_t kchannel_ref_t;
typedef struct _address_t kaddress_t;
typedef struct _stream_t kstream_t;

/* 管道可投递事件 */
typedef enum _channel_event_e {
    channel_event_recv = 1,
    channel_event_send = 2,
} knet_channel_event_e;

/*! 管道状态 */
typedef enum _channel_state_e {
    channel_state_connect = 1, /*! 主动发起连接，连接未完成 */
    channel_state_accept = 2,  /*! 监听 */
    channel_state_close = 4,   /*! 管道已关闭 */
    channel_state_active = 8,  /*! 管道已激活，可以收发数据 */
    channel_state_init = 16,   /*! 管道已建立，但未连接 */
} knet_channel_state_e;

/* 错误码 */
typedef enum _error_e {
    error_ok = 0,
    error_fail,
    error_invalid_parameters,
    error_must_be_shared_channel_ref,
    error_invalid_channel,
    error_invalid_broadcast,
    error_no_memory,
    error_hash_not_found,
    error_recv_fail,
    error_send_fail,
    error_send_patial,
    error_recv_buffer_full,
    error_recv_nothing,
    error_connect_fail,
    error_connect_in_progress,
    error_channel_not_connect,
    error_accept_in_progress,
    error_bind_fail,
    error_listen_fail,
    error_ref_nonzero,
    error_loop_fail,
    error_loop_attached,
    error_loop_not_found,
    error_loop_impl_init_fail,
    error_thread_start_fail,
    error_already_close,
    error_impl_add_channel_ref_fail,
    error_broadcast_not_found,
    error_getpeername,
    error_getsockname,
    error_not_correct_domain,
    error_multiple_start,
    error_not_connected,
    error_logger_write,
    error_set_tls_fail,
    error_recvbuffer_not_enough,
    error_recvbuffer_locked,
    error_stream_enable,
    error_stream_disable,
    error_stream_flush,
    error_stream_buffer_overflow,
    error_trie_not_found,
    error_trie_key_exist,
    error_trie_for_each_fail,
    error_ip_filter_open_fail,
    error_router_wire_not_found,
    error_router_wire_exist,
    error_ringbuffer_not_found,
    error_getaddrinfo_fail,
} knet_error_e;

/*! 管道回调事件 */
typedef enum _channel_cb_event_e {
    channel_cb_event_connect = 1,          /*! 连接完成 */
    channel_cb_event_accept = 2,           /*! 管道监听到了新连接请求 */ 
    channel_cb_event_recv = 4,             /*! 管道有数据可以读 */
    channel_cb_event_send = 8,             /*! 管道发送了字节，保留 */
    channel_cb_event_close = 16,           /*! 管道关闭 */
    channel_cb_event_timeout = 32,         /*! 管道读空闲 */
    channel_cb_event_connect_timeout = 64, /*! 主动发起连接，但连接超时 */
} knet_channel_cb_event_e;

/* 日志等级 */
typedef enum _logger_level_e {
    logger_level_verbose = 1, /* verbose - 尽量输出 */
    logger_level_information, /* information - 提示信息 */
    logger_level_warning,     /* warning - 警告 */ 
    logger_level_error,       /* error - 错误 */
    logger_level_fatal,       /* fatal - 致命错误 */
} knet_logger_level_e;

/* 日志模式 */
typedef enum _logger_mode_e {
    logger_mode_file = 1,     /* 生成日志文件 */
    logger_mode_console = 2,  /* 打印到stderr */
    logger_mode_flush = 4,    /* 每次写日志同时清空缓存 */
    logger_mode_override = 8, /* 覆盖已存在的日志文件 */
} knet_logger_mode_e;

/*! 管道事件回调函数 */
typedef void (*knet_channel_ref_cb_t)(kchannel_ref_t*, knet_channel_cb_event_e);

/**
 * 取得IP
 * @param address kaddress_t实例
 * @retval 有效的指针 IP字符串
 * @retval 0 管道连接未建立
 */
extern const char* address_get_ip(kaddress_t* address);

/**
 * 取得port
 * @param address kaddress_t实例
 * @retval 有效的端口号 端口号
 * @retval 0 管道连接未建立
 */
extern int address_get_port(kaddress_t* address);

/**
 * 测试是否相等
 * @param address kaddress_t实例
 * @param ip IP
 * @param port 端口
 * @retval 0 相等
 * @retval 非零 不相等
 */
extern int address_equal(kaddress_t* address, const char* ip, int port);

/**
 * 创建一个事件循环
 * @return kloop_t实例
 */
extern kloop_t* knet_loop_create();

/**
 * 销毁事件循环
 * 事件循环内的所有管道也会被销毁
 * @param loop kloop_t实例
 */
extern void knet_loop_destroy(kloop_t* loop);

/**
 * 创建管道
 * @param loop kloop_t实例
 * @param max_send_list_len 发送缓冲区链最大长度
 * @param recv_ring_len 接受环形缓冲区最大长度
 * @return kchannel_ref_t实例
 */
extern kchannel_ref_t* knet_loop_create_channel(kloop_t* loop, uint32_t max_send_list_len, uint32_t recv_ring_len);

/**
 * 创建管道
 * @param loop kloop_t实例
 * @param max_send_list_len 发送缓冲区链最大长度
 * @param recv_ring_len 接受环形缓冲区最大长度
 * @return kchannel_ref_t实例
 */
extern kchannel_ref_t* knet_loop_create_channel6(kloop_t* loop, uint32_t max_send_list_len, uint32_t recv_ring_len);

/**
 * 运行一次事件循环
 * kloop_t不是线程安全的，不能在多个线程内同时对同一个kloop_t实例调用knet_loop_run_once
 * @param loop kloop_t实例
 * @retval error_ok 成功
 * @retval 其他 失败
 */
extern int knet_loop_run_once(kloop_t* loop);

/**
 * 运行事件循环直到调用knet_loop_exit()
 * kloop_t不是线程安全的，不能在多个线程内同时对同一个kloop_t实例调用knet_loop_run
 * @param loop kloop_t实例
 * @retval error_ok 成功
 * @retval 其他 失败
 */
extern int knet_loop_run(kloop_t* loop);

/**
 * 退出函数knet_loop_run()
 * @param loop kloop_t实例
 */
extern void knet_loop_exit(kloop_t* loop);

/**
 * 获取活跃管道数量
 * @param loop kloop_t实例
 * @return 活跃管道数量
 */
extern int knet_loop_get_active_channel_count(kloop_t* loop);

/**
 * 获取已关闭管道数量
 * @param loop kloop_t实例
 * @return 关闭管道数量
 */
extern int knet_loop_get_close_channel_count(kloop_t* loop);

/**
 * 增加管道引用计数，并创建与管道关联的新的kchannel_ref_t实例
 *
 * knet_channel_ref_share调用完成后，可以在当前线程内访问其他线程(kloop_t)内运行的管道
 * @param channel_ref kchannel_ref_t实例
 * @return kchannel_ref_t实例
 */
extern kchannel_ref_t* knet_channel_ref_share(kchannel_ref_t* channel_ref);

/**
 * 减少管道引用计数，并销毁kchannel_ref_t实例
 * @param channel_ref kchannel_ref_t实例
 */
extern void knet_channel_ref_leave(kchannel_ref_t* channel_ref);

/**
 * 将管道转换为监听管道
 *
 * 由这个监听管道接受的新连接将使用与监听管道相同的发送缓冲区最大数量限制和接受缓冲区长度限制,
 * knet_channel_ref_accept所接受的新连接将被负载均衡，实际运行在哪个kloop_t内依赖于实际运行的情况
 * @param channel_ref kchannel_ref_t实例
 * @param ip IP
 * @param port 端口
 * @param backlog 等待队列上限（listen())
 * @retval error_ok 成功
 * @retval 其他 失败
 */
extern int knet_channel_ref_accept(kchannel_ref_t* channel_ref, const char* ip, int port, int backlog);

/**
 * 主动连接
 *
 * 调用knet_channel_ref_connect的管道会被负载均衡，实际运行在哪个kloop_t内依赖于实际运行的情况
 * @param channel_ref kchannel_ref_t实例
 * @param ip IP
 * @param port 端口
 * @param timeout 连接超时（秒）
 * @retval error_ok 成功
 * @retval 其他 失败
 */
extern int knet_channel_ref_connect(kchannel_ref_t* channel_ref, const char* ip, int port, int timeout);

/**
 * 重新发起连接
 *
 * <pre>
 * 超时的管道将被关闭，建立新管道重连，新管道将使用原有管道的属性，包含回调函数和用户指针
 * 如果timeout设置为0，则使用原有的连接超时，如果timeout>0则使用新的连接超时
 * </pre>
 * @param channel_ref kchannel_ref_t实例
 * @param timeout 连接超时（秒）
 * @retval error_ok 成功
 * @retval 其他 失败
 */
extern int knet_channel_ref_reconnect(kchannel_ref_t* channel_ref, int timeout);

/**
 * 设置管道自动重连
 * <pre>
 * auto_reconnect为非零值则开启自动重连，所有非错误性导致管道关闭，都会自动重连，用户手动调用
 * knet_channel_ref_close将不会触发自动重连
 * </pre>
 * @param channel_ref kchannel_ref_t实例
 * @param auto_reconnect 自动重连标志
 */
extern void knet_channel_ref_set_auto_reconnect(kchannel_ref_t* channel_ref, int auto_reconnect);

/**
 * 检查管道是否开启了自动重连
 * @param channel_ref kchannel_ref_t实例
 * @retval 0 未开启
 * @retval 非零 开启
 */
extern int knet_channel_ref_check_auto_reconnect(kchannel_ref_t* channel_ref);

/**
 * 检测管道是否是通过负载均衡关联到当前的kloop_t
 * @param channel_ref kchannel_ref_t实例
 * @retval 0 不是
 * @retval 非0 是
 */
extern int knet_channel_ref_check_balance(kchannel_ref_t* channel_ref);

/**
 * 检测管道当前状态
 * @param channel_ref kchannel_ref_t实例
 * @param state 需要测试的状态
 * @retval 1 是
 * @retval 0 不是
 */
extern int knet_channel_ref_check_state(kchannel_ref_t* channel_ref, knet_channel_state_e state);

/**
 * 关闭管道
 * @param channel_ref kchannel_ref_t实例
 */
extern void knet_channel_ref_close(kchannel_ref_t* channel_ref);

/**
 * 检查管道是否已经关闭
 * @param channel_ref kchannel_ref_t实例
 * @retval 0 未关闭
 * @retval 非零 关闭
 */
extern int knet_channel_ref_check_close(kchannel_ref_t* channel_ref);

/**
 * 取得管道数据流
 * @param channel_ref kchannel_ref_t实例
 * @return kstream_t实例
 */
extern kstream_t* knet_channel_ref_get_stream(kchannel_ref_t* channel_ref);

/**
 * 取得管道所关联的事件循环
 * @param channel_ref kchannel_ref_t实例
 * @return kloop_t实例
 */
extern kloop_t* knet_channel_ref_get_loop(kchannel_ref_t* channel_ref);

/**
 * 设置管道事件回调
 *
 * 事件回调将在关联的kloop_t实例所在线程内被回调
 * @param channel_ref kchannel_ref_t实例
 * @param cb 回调函数
 */
extern void knet_channel_ref_set_cb(kchannel_ref_t* channel_ref, knet_channel_ref_cb_t cb);

/**
 * 设置管道空闲超时
 *
 * 管道空闲超时依赖读操作作为判断，在timeout间隔内未有可读数据既触发超时
 * @param channel_ref kchannel_ref_t实例
 * @param timeout 超时（秒）
 */
extern void knet_channel_ref_set_timeout(kchannel_ref_t* channel_ref, int timeout);

/**
 * 取得对端地址
 * @param channel_ref kchannel_ref_t实例
 * @return kaddress_t实例
 */
extern kaddress_t* knet_channel_ref_get_peer_address(kchannel_ref_t* channel_ref);

/**
 * 取得本地地址
 * @param channel_ref kchannel_ref_t实例
 * @return kaddress_t实例
 */
extern kaddress_t* knet_channel_ref_get_local_address(kchannel_ref_t* channel_ref);

/**
 * 获取管道UUID
 * @param channel_ref kchannel_t实例
 * @return 管道UUID
 */
extern uint64_t knet_channel_ref_get_uuid(kchannel_ref_t* channel_ref);

/**
 * 测试两个管道引用是否指向同一个管道
 * @param a kchannel_t实例
 * @param b kchannel_t实例
 * @retval 0 不同
 * @retval 非零 相同 
 */
extern int knet_channel_ref_equal(kchannel_ref_t* a, kchannel_ref_t* b);

/**
 * 设置用户数据指针
 * @param channel_ref kchannel_t实例
 * @param ptr 用户数据指针
 */
extern void knet_channel_ref_set_ptr(kchannel_ref_t* channel_ref, void* ptr);

/**
 * 获取用户数据指针
 * @param channel_ref kchannel_t实例
 * @return 用户数据指针
 */
extern void* knet_channel_ref_get_ptr(kchannel_ref_t* channel_ref);

/**
 * 递增当前管道引用计数
 * @param channel_ref kchannel_ref_t实例
 * @return 当前引用计数
 */
extern int knet_channel_ref_incref(kchannel_ref_t* channel_ref);

/**
 * 递减当前管道引用计数
 * @param channel_ref kchannel_ref_t实例
 * @return 当前引用计数
 */
extern int knet_channel_ref_decref(kchannel_ref_t* channel_ref);

/**
 * 检测是否是IPV6管道
 * @param channel_ref kchannel_ref_t实例
 * @retvel 0 不是
 * @retval 非0 是
 */
extern int knet_channel_ref_is_ipv6(kchannel_ref_t* channel_ref);

/**
 * 设置reuseport
 * @param channel_ref kchannel_ref_t实例
 * @retvel 0 不是
 * @retval 非0 是
 */
extern int knet_channel_ref_set_reuseport(kchannel_ref_t* channel_ref);

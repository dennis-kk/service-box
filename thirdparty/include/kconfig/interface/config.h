/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef KCONFIG_H
#define KCONFIG_H

#include <cstdint>
#include <functional>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

/**
 * @defgroup Config 配置器组件
 * 配置器组件
 *
 * <pre>
 * 配置器提供了一种通用的配置文件格式，格式本身简单同时又可以表示复杂的配置的结构。
 * 配置器支持以下几种类型:
 * 1. 域
 * 2. 数字
 * 3. 字符串
 * 4. 数组
 * 5. 表
 *
 * <b>域</b>
 * 域是一种递归结构，对应的接口为ZoneAttribute:
 * level1 = {
 *    a = 1
 *    b = "string"
 *    level2 = {
 *    }
 * }
 * 如上所示，域内可以包含所有其他类型，可以根据配置的不同用途使用域作分割。
 *
 * <b>数字</b>
 * 数字包括所有基本整数类型，包含有符号和无符号，还包含单精度和双精度浮点数。
 *
 * <b>字符串</b>
 * 通常意义上的字符串，必须使用ascii码""来包裹。
 *
 * <b>数组</b>
 * 数组内元素是除了域意外的其他任意类型，类型可以不同。
 *
 * array = (1, "2", 3.0, (1,2), "4", true, false)
 *
 * 如上所示：数组内有5个元素，分别是数字1，字符串"2"，浮点数3.0，数组(1,2)，字符串"4",
 * 布尔值真，布尔值假。
 *
 * <b>表</b>
 * 表是比较特殊的类型，有多个键-值对组成，键必须是字符串类型，但是不能使用""包裹，值的类型
 * 是除去域以外的任意类型。
 *
 * table = <1:1, 2:"2", 3:3.0, 4:(1,2)>
 *
 * 如上所示，表内的所有键没有使用""包裹，值的类型有多种。
 *
 * <b>访问配置的内容</b>
 * 使用点分路径的格式来访问配置属性:
 *
 * a = {
 *    b = {
 *       number = 1
 *       string = "string"
 *       array = (1, 2, 3, 4)
 *       table = <1:1, 2:2, 3:3>
 *       bool = true
 *    }
 * }
 *
 * 如上所示的示例，访问number的路径为a.b.number，访问table的路径为a.b.table，路径直接反映了
 * 配置文件的结构。
 *
 * <b>属性接口</b>
 *  1. Attribute        属性根接口
 *  2. NumberAttribute  数字接口
 *  3. StringAttribute  字符串接口
 *  4. ArrayAttribute   数组接口
 *  5. TableAttribute   表接口
 *  6. TablePair        键-值对接口
 *  7. ZoneAttribute    域接口
 *  8. BoolAttribute    布尔值接口
 *  9. NullAttribute    空值接口
 *
 * Attribute作为根接口提供了转换到其他类型的方法，同时提供了确定属性类型的方法。
 * 其他接口提供了具体类型的属性的方法集。具体接口说明请接口文档。
 *
 * </pre>
 */

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
#elif __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Woverloaded-virtual"
#endif

class Domain;

namespace kconfig {

class Attribute;
class NumberAttribute;
class StringAttribute;
class ArrayAttribute;
class TableAttribute;
class BoolAttribute;
class TablePair;
class ZoneAttribute;
class NullAttribute;
class ConfigException;

using AttributePtr = std::shared_ptr<Attribute>;

/**
 * 配置组件异常
 */
class ConfigException : public std::logic_error {
public:
  explicit ConfigException(const std::string &reason)
      : logic_error(reason.c_str()) { // construct from message string
  }

  explicit ConfigException(const char *reason)
      : logic_error(reason) { // construct from message string
  }
};

/**
 * @brief 属性根接口
 */
class Attribute {
public:
  static const int NULL_VAL = 0; ///< null
  static const int NUMBER = 1;   ///< numeric
  static const int STRING = 2;   ///< string
  static const int ARRAY = 3;    ///< array
  static const int TABLE = 4;    ///< table
  static const int ZONE = 5;     ///< zone
  static const int BOOL = 6;     ///< bool

public:
  /**
   * 虚析构实现
   */
  virtual ~Attribute() {}

  /**
   * 测试是否是数字类型
   * @retval true 是
   * @retval false 否
   */
  virtual bool isNumber() = 0;

  /**
   * 测试是否是字符串类型
   * @retval true 是
   * @retval false 否
   */
  virtual bool isString() = 0;

  /**
   * 测试是否是数组类型
   * @retval true 是
   * @retval false 否
   */
  virtual bool isArray() = 0;

  /**
   * 测试是否是表类型
   * @retval true 是
   * @retval false 否
   */
  virtual bool isTable() = 0;

  /**
   * 测试是否是域类型
   * @retval true 是
   * @retval false 否
   */
  virtual bool isZone() = 0;

  /**
   * 测试是否是布尔
   * @retval true 是
   * @retval false 否
   */
  virtual bool isBool() = 0;

  /**
   * 测试是否是空
   * @retval true 是
   * @retval false 否
   */
  virtual bool isNull() = 0;

  /**
   * 转换为数字接口，如果类型不匹配抛出异常
   * @return 数字接口
   */
  virtual NumberAttribute *number() = 0;

  /**
   * 转换为字符串接口，如果类型不匹配抛出异常
   * @return 字符串接口
   */
  virtual StringAttribute *string() = 0;

  /**
   * 转换为数组接口，如果类型不匹配抛出异常
   * @return 数组接口
   */
  virtual ArrayAttribute *array() = 0;

  /**
   * 转换为表接口，如果类型不匹配抛出异常
   * @return 表接口
   */
  virtual TableAttribute *table() = 0;

  /**
   * 转换为域接口，如果类型不匹配抛出异常
   * @return 域接口
   */
  virtual ZoneAttribute *zone() = 0;
  /**
   * 转换为布尔接口，如果类型不匹配抛出异常
   * @return 布尔接口
   */
  virtual BoolAttribute *boolean() = 0;
  /*
   * 获取名字
   */
  virtual const std::string &name() = 0;
  /*
   * 设置名字
   */
  virtual auto set_name(const std::string &name) -> void = 0;
  /*
   * 获取类型
   */
  virtual int getType() = 0;
  /*
   * 表达式计算
   *
   * @param op 运算符
   * @param rhs 运算符右边表达式
   */
  virtual AttributePtr doOperate(int op, AttributePtr rhs) = 0;
  /*
   * 转化字符串
   */
  virtual auto to_string() -> std::string = 0;
};

/**
 * 空值接口
 */
class NullAttribute : public Attribute {
public:
  virtual ~NullAttribute() {}
};

/**
 * 数字接口
 */
class NumberAttribute : public Attribute {
public:
  /**
   * 虚析构实现
   */
  virtual ~NumberAttribute() {}

  /**
   * 取得int
   * @return int
   */
  virtual int getInt() = 0;

  /**
   * 取得配置字符串
   * @return 配置字符串
   */
  virtual const std::string &getString() = 0;

  /**
   * 取得unsigned int
   * @return unsigned int
   */
  virtual unsigned int getUint() = 0;

  /**
   * 取得long
   * @return long
   */
  virtual long getLong() = 0;

  /**
   * 取得unsigned long
   * @return unsigned long
   */
  virtual unsigned long getUlong() = 0;

  /**
   * 取得long long
   * @return long long
   */
  virtual long long getLlong() = 0;

  /**
   * 取得unsigned long long
   * @return unsigned long long
   */
  virtual unsigned long long getULlong() = 0;

  /**
   * 取得float
   * @return float
   */
  virtual float getFloat() = 0;

  /**
   * 取得double
   * @return double
   */
  virtual double getDouble() = 0;
};

/**
 * 字符串接口
 */
class StringAttribute : public Attribute {
public:
  /**
   * 虚析构实现
   */
  virtual ~StringAttribute() {}

  /**
   * 取得字符串
   * @return 字符串
   */
  virtual const std::string &get() = 0;
};
/*
 * 布尔接口
 */
class BoolAttribute : public Attribute {
public:
  /**
   * 虚析构实现
   */
  virtual ~BoolAttribute() {}

  /**
   * 取得布尔值
   * @return 布尔值
   */
  virtual bool get() = 0;
};

/**
 * 数组接口
 */
class ArrayAttribute : public Attribute {
public:
  using Attribute::array;
  using Attribute::boolean;
  using Attribute::number;
  using Attribute::string;
  using Attribute::table;

  /**
   * 虚析构实现
   */
  virtual ~ArrayAttribute() {}

  /**
   * 取得数组内元素个数
   * @return 数组内元素个数
   */
  virtual int getSize() = 0;

  /**
   * 按下表取得数组元素，并转换为NumberAttribute
   * @param index 数组下表
   * @return NumberAttribute
   */
  virtual NumberAttribute *number(int index) = 0;

  /**
   * 按下表取得数组元素，并转换为StringAttribute
   * @param index 数组下表
   * @return StringAttribute
   */
  virtual StringAttribute *string(int index) = 0;

  /**
   * 按下表取得数组元素，并转换为ArrayAttribute
   * @param index 数组下表
   * @return ArrayAttribute
   */
  virtual ArrayAttribute *array(int index) = 0;

  /**
   * 按下表取得数组元素，并转换为TableAttribute
   * @param index 数组下表
   * @return TableAttribute
   */
  virtual TableAttribute *table(int index) = 0;
  /**
   * 按下表取得数组元素，并转换为BoolAttribute
   * @param index 数组下表
   * @return BoolAttribute
   */
  virtual BoolAttribute *boolean(int index) = 0;
  /**
   * 按下表取得数组元素
   * @param index 数组下表
   * @return Attribute
   */
  virtual Attribute *get(int index) = 0;
  /**
   * 按下表取得数组元素
   * @param index 数组下表
   * @return Attribute
   */
  virtual AttributePtr get_ptr(int index) = 0;
};

/**
 * 键-值接口
 */
class TablePair {
public:
  /**
   * 虚析构实现
   */
  virtual ~TablePair() {}

  /**
   * 取得key
   * @return key
   */
  virtual const std::string &key() = 0;

  /**
   * 取得值
   * @return 值
   */
  virtual Attribute *value() = 0;
  /**
   * 取得值
   * @return 值
   */
  virtual AttributePtr value_ptr() = 0;
};

/**
 * 表接口
 */
class TableAttribute : public Attribute {
public:
  /**
   * 虚析构实现
   */
  virtual ~TableAttribute() {}

  /**
   * 取得表内元素个数
   * @return 表内元素个数
   */
  virtual int getSize() = 0;

  /**
   * 取得表键-值对
   * @param key 键
   * @return 键-值对
   */
  virtual TablePair *get(const std::string &key) = 0;
  /**
   * 取得值
   * @param key 键
   * @return 值
   */
  virtual AttributePtr get_ptr(const std::string &key) = 0;

  /**
   * 遍历器是否还有元素可以遍历
   * @retval true 有
   * @retval false 没有
   */
  virtual bool hasNext() = 0;

  /**
   * 取得遍历器下一个元素
   * @return 键-值对
   */
  virtual TablePair *next() = 0;
};

/**
 * 域接口
 */
class ZoneAttribute : public Attribute {
public:
  /**
   * 虚析构实现
   */
  virtual ~ZoneAttribute() {}

  /**
   * 测试域内是否有属性
   * @param name 属性名
   * @retval true 有
   * @retval false 没有
   */
  virtual bool has(const std::string &name) = 0;

  /**
   * 根据名字取得属性
   * @return 属性
   */
  virtual Attribute *get(const std::string &name) = 0;

  /**
   * 遍历器是否还有元素可以遍历
   * @retval true 有
   * @retval false 没有
   */
  virtual bool hasNext() = 0;

  /**
   * 取得遍历器下一个元素
   * @return 属性
   */
  virtual Attribute *next() = 0;
};

using ConfigFunction =
    std::function<AttributePtr(const std::vector<AttributePtr> &)>;

/**
 * 配置接口
 */
class Config {
public:
  virtual ~Config() {}

  /**
   * 加载
   * @param base 配置文件所在目录
   * @param file 文件名
   */
  virtual void load(const std::string &base, const std::string &file) = 0;

  /**
   * 重新加载
   * @param base 配置文件所在目录
   * @param file 文件名
   */
  virtual void reload(const std::string &base, const std::string &file) = 0;

  /**
   * 重新加载
   */
  virtual void reload() = 0;

  /**
   * 卸载
   */
  virtual void unload() = 0;

  /**
   * 指定的key是否存在
   * @retval true 存在
   * @retval false 不存在
   */
  virtual bool has(const std::string &key) = 0;

  /**
   * 取得整数配置
   * @param key 键
   * @return 值
   */
  virtual int8_t getInt8(const std::string &key) = 0;

  /**
   * 取得整数配置
   * @param key 键
   * @return 值
   */
  virtual uint8_t getUint8(const std::string &key) = 0;

  /**
   * 取得整数配置
   * @param key 键
   * @return 值
   */
  virtual int16_t getInt16(const std::string &key) = 0;

  /**
   * 取得整数配置
   * @param key 键
   * @return 值
   */
  virtual uint16_t getUint16(const std::string &key) = 0;

  /**
   * 取得整数配置
   * @param key 键
   * @return 值
   */
  virtual int32_t getInt32(const std::string &key) = 0;

  /**
   * 取得整数配置
   * @param key 键
   * @return 值
   */
  virtual uint32_t getUint32(const std::string &key) = 0;

  /**
   * 取得整数配置
   * @param key 键
   * @return 值
   */
  virtual int64_t getInt64(const std::string &key) = 0;

  /**
   * 取得整数配置
   * @param key 键
   * @return 值
   */
  virtual uint64_t getUint64(const std::string &key) = 0;

  /**
   * 取得浮点配置
   * @param key 键
   * @return 值
   */
  virtual float getFloat(const std::string &key) = 0;

  /**
   * 取得浮点配置
   * @param key 键
   * @return 值
   */
  virtual double getDouble(const std::string &key) = 0;

  /**
   * 取得字符串配置
   * @param key 键
   * @return 值
   */
  virtual const std::string &getString(const std::string &key) = 0;

  /**
   * 取得布尔值配置
   * @param key 键
   * @return 布尔值
   */
  virtual bool getBool(const std::string &key) = 0;

  /**
   * 取得属性，并返回Attribute接口
   * @param key 键
   * @return Attribute接口
   */
  virtual Attribute *get(const std::string &key) = 0;

  /**
   * 取得属性，并返回NumberAttribute接口
   * @param key 键
   * @return NumberAttribute接口
   */
  virtual NumberAttribute *number(const std::string &key) = 0;

  /**
   * 取得属性，并返回StringAttribute接口
   * @param key 键
   * @return StringAttribute接口
   */
  virtual StringAttribute *string(const std::string &key) = 0;

  /**
   * 取得属性，并返回ArrayAttribute接口
   * @param key 键
   * @return ArrayAttribute接口
   */
  virtual ArrayAttribute *array(const std::string &key) = 0;

  /**
   * 取得属性，并返回TableAttribute接口
   * @param key 键
   * @return TableAttribute接口
   */
  virtual TableAttribute *table(const std::string &key) = 0;

  /**
   * 取得属性，并返回ZoneAttribute接口
   * @param key 键
   * @return ZoneAttribute接口
   */
  virtual ZoneAttribute *zone(const std::string &key) = 0;
  /**
   * 取得属性，并返回BoolAttribute接口
   * @param key 键
   * @return BoolAttribute接口
   */
  virtual BoolAttribute *boolean(const std::string &key) = 0;
  /*
   * 注册函数
   *
   * @param name 函数名
   * @param func 函数实现
   * @return true 成功, false 失败
   */
  virtual bool register_function(const std::string &name,
                                 ConfigFunction func) = 0;
  /*
   * 反注册函数
   *
   * @param name 函数名
   */
  virtual void unregister_function(const std::string &name) = 0;
  /*
   * 获取函数
   *
   * @param name 函数名
   * @return 函数实现
   */
  virtual ConfigFunction get_function(const std::string &name) = 0;
  /*
   * 获取root domain
   *
   * @return root domain
   */
  virtual Domain *get_root_domain() = 0;
};

} // namespace kconfig

#ifdef __clang__
#pragma clang diagnostic pop
#elif __GUNC__
#pragma GCC diagnostic pop
#endif

/** @} */

#endif // KCONFIG_H

﻿#pragma once
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace kratos {
namespace loadbalance {

/**
 * @enum BalancerMod
 * @brief 负载均衡器的枚举类型
 */
enum class BalancerMod : std::uint8_t {
  None_Scheduling = 0,
  /**
   * IP哈希
   * 用户在调用get_next的时候需要传入此时调用者的ip作为哈希的value
   */
  Ip_Hash_Scheduling,
  /**
   * 最少链接数
   * 每次会选取最少链接的一个节点，若存在多个相同链接数的最小节点，会从当中随机算出一个。
   */
  Least_Active_Scheduling,
  /**
   * 轮询
   */
  Round_Robin_Scheduling,
  /**
   * 带权重的轮询
   * 每个节点会按照权重的大小进行轮询，优先寻找权重最大的。如果所有的节点的权重都一样，就相当于是轮询
   */
  Weighted_Round_Robin_Scheduling,
  /**
   * 平滑的带权重的轮询
   * 如果服务器的权重差别很大，出于平滑的考虑，避免短时间内会节点造成冲击，你可以选择该算法
   * 如果服务器的差别不是很大，可以考虑使用Weighted_Round_Robin_Scheduling的算法，因为它的性能要好于该算法:
   */
  Smooth_Weighted_Round_Robin_Scheduling,
  /**
   * 带虚拟节点的平滑轮询
   */
  Virtual_Node_Smooth_Weighted_Round_Robin_Scheduling,
};

/**
 * @class ILBNode load_balance.h load_balance.h
 * @brief 负载均衡器节点的虚基类
 * @note 用户可以继承
 * @details
 * 所有的节点交由用户管理生命周期，LoadBalancer只是负责提供算法，因此std::weak_ptr来进行封装
 */
class ILBNode {
public:
  ILBNode() = default;
  virtual ~ILBNode() = default;

public:
  /**
   * 设置节点的名字
   * @param name 节点的名字
   * @details
   * 节点的唯一识别码。均衡器会根据节点的名字做映射，来进行相对应的更新操作。
   */
  virtual auto set_name(const std::string &name) -> void = 0;
  /**
   * 获取节点的名字
   * @return 节点的名字
   */
  virtual auto get_name() -> const std::string & = 0;
  /**
   * 设置节点的权重
   * @param weight 节点的权重
   * @details 两种带权重的轮询必须要设置权重。如果不设置的话，就是普通的轮询。
   */
  virtual auto set_weight(std::int32_t weight) -> void = 0;
  /**
   * 获取节点权重
   * @return 节点权重
   */
  virtual auto get_weight() -> std::int32_t = 0;
  /**
   * 设置实时活跃的链接数
   * @param 实时链接数
   * @details 最少链接数的负载均衡器 必须要根据实际情况设置链接数
   */
  virtual auto set_active_count(std::int32_t count) -> void = 0;
  /**
   * 获取实时活跃的链接数
   * @return 返回试试的活跃链接数
   */
  virtual auto get_active_count() -> std::int32_t = 0;
};

using LoadBalanceNodeWeakPtr = std::weak_ptr<ILBNode>;

/**
 * @class ILoadBalancer load_balance.h load_balance.h
 * @brief 负载均衡器的虚基类
 * @note 用户在使用工厂类的时候可以拿到 虚基类的std::unique_ptr
 * @details
 * 所有的负载均衡器交由用户管理生命周期，LoadBalancer只是负责提供算法，用户调用虚基类的四个接口进行维护
 */
class ILoadBalancer {
public:
  ILoadBalancer() = default;
  virtual ~ILoadBalancer() = default;

public:
  /**
   * 增加可以用的节点
   * @param lbnode 可用节点的
   * @see LoadBalanceNodeWeakPtr
   */
  virtual auto add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool = 0;
  /**
   * 获取下一个节点
   * @param ip 客户端的ip默认值为空，仅仅当使用ip哈希的负载均衡时才需要填写
   * @return LoadBalanceNodeWeakPtr 节点的weakptr
   * @details 用户拿到weakptr的时候需要进行expired判断。可能会返回空的weakptr。
   *         \ 原因：1. 节点实际已经失效但是并未从均衡器中删除。
   */
  virtual auto get_next(const std::string &ip = "")
      -> LoadBalanceNodeWeakPtr = 0;
  /**
   * 清除均衡器当中的所有节点
   */
  virtual auto clear() -> void = 0;
  /**
   * 刷新节点信息
   * @details 主动更新了节点得“权重”的时候 需要让均衡器重新计算
   */
  virtual auto reset_balancer() -> void = 0;
};
/**
 * @class BalancerFactory load_balance.h load_balance.h
 * @brief 负载均衡器的工厂类
 * @note 静态方法类
 * @details
 * 仅仅提供两个方法，注册和创建。一共注册了五种不通的均衡器。用户在使用的时候可以去创建，每次创建都是不一样的对象实例。
 */

class BalancerFactory {
public:
  BalancerFactory() = delete;
  BalancerFactory(const BalancerFactory &) = delete;
  BalancerFactory(BalancerFactory &&) = delete;
  BalancerFactory &operator=(const BalancerFactory &) = delete;

  using ICreateMethod = std::unique_ptr<ILoadBalancer> (*)();
  ICreateMethod i_creater_method_;

private:
  using BalancersMap = std::map<std::underlying_type<BalancerMod>::type,
                                BalancerFactory::ICreateMethod>;
  /**
   * 获取均衡器的创建容器
   * @return 返回均衡器的容器
   * @details 为了解决静态初始化的相依性
   */
  static auto get_balancer_map() -> BalancersMap &;

public:
  /**
   * 注册均衡器
   * @param mod 均衡器的模式
   * @param t 均衡器的创建函数
   * @return 是否注册成功
   * @retval true 创建成功
   * @retval false 创建失败
   */
  static auto register_balancer(BalancerMod mod, ICreateMethod t) -> bool;

  /**
   * 创建均衡器
   * @param mod 均衡器的模式
   * @return 均衡器的uniqueptr
   */
  static auto create_balancer(BalancerMod mod)
      -> std::unique_ptr<ILoadBalancer>;
};

class IPHASHSBalancer : public ILoadBalancer {
public:
  IPHASHSBalancer() = default;
  virtual ~IPHASHSBalancer() = default;

public:
  virtual auto add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool override;
  virtual auto get_next(const std::string &ip = "")
      -> LoadBalanceNodeWeakPtr override;
  virtual auto clear() -> void override;
  virtual auto reset_balancer() -> void override{};

  static auto get_name() -> const std::string { return "IP_HASH_SCHEDULING"; };
  static auto get_mod() -> BalancerMod {
    return BalancerMod::Ip_Hash_Scheduling;
  }
  static auto creator() -> std::unique_ptr<ILoadBalancer> {
    return std::make_unique<IPHASHSBalancer>();
  }

private:
  static bool ip_hash_register_;
  std::int32_t server_count_{0};
  std::vector<LoadBalanceNodeWeakPtr> nodes_;
};

class LASBalancer : public ILoadBalancer {
public:
  LASBalancer() = default;
  virtual ~LASBalancer() = default;

public:
  virtual auto add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool override;
  virtual auto get_next(const std::string &ip = "")
      -> LoadBalanceNodeWeakPtr override;
  virtual auto clear() -> void override;
  virtual auto reset_balancer() -> void override{};

  static auto get_name() -> const std::string {
    return "LEAST_ACTIVE_BALANCER";
  }
  static auto get_mod() -> BalancerMod {
    return BalancerMod::Least_Active_Scheduling;
  }
  static auto creator() -> std::unique_ptr<ILoadBalancer> {
    return std::make_unique<LASBalancer>();
  }

private:
  static bool least_active_register_;
  std::int32_t server_count_{0};
  std::vector<LoadBalanceNodeWeakPtr> nodes_;
};

class RRSBalancer : public ILoadBalancer {
public:
  RRSBalancer() = default;
  virtual ~RRSBalancer() = default;

public:
  virtual auto add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool override;
  virtual auto get_next(const std::string &ip = "")
      -> LoadBalanceNodeWeakPtr override;
  virtual auto clear() -> void override;
  virtual auto reset_balancer() -> void override{};

  static auto get_name() -> const std::string { return "ROUND_ROBIN_BALANCER"; }
  static auto get_mod() -> BalancerMod {
    return BalancerMod::Round_Robin_Scheduling;
  }
  static auto creator() -> std::unique_ptr<ILoadBalancer> {
    return std::make_unique<RRSBalancer>();
  }

private:
  static bool round_robin_register_;
  std::int32_t last_select_index_{-1};
  std::int32_t server_count_{0};
  std::vector<LoadBalanceNodeWeakPtr> nodes_;
};

class WRRSBalancer : public ILoadBalancer {
public:
  WRRSBalancer() = default;
  virtual ~WRRSBalancer() = default;

public:
  virtual auto add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool override;
  virtual auto get_next(const std::string &ip = "")
      -> LoadBalanceNodeWeakPtr override;
  virtual auto clear() -> void override;
  virtual auto reset_balancer() -> void override;

  static auto get_name() -> const std::string {
    return "WEIGHTED_ROUND_ROBIN_SCHEDULING";
  }
  static auto get_mod() -> BalancerMod {
    return BalancerMod::Weighted_Round_Robin_Scheduling;
  }
  static auto creator() -> std::unique_ptr<ILoadBalancer> {
    return std::make_unique<WRRSBalancer>();
  }

private:
  static bool weight_round_robin_register_;
  std::int32_t last_select_index_{-1};
  std::int32_t tot_weight_{0};
  std::int32_t max_weight_{0};
  std::int32_t current_weight_{0};
  std::int32_t gcd_weight_{0};
  std::int32_t server_count_{0};
  std::vector<LoadBalanceNodeWeakPtr> nodes_;
};

struct SWRRSNode {
  SWRRSNode() = default;
  SWRRSNode(LoadBalanceNodeWeakPtr n) : lb_node(n), cur_weight(0) {
    effect_weight = lb_node.lock()->get_weight();
  }
  LoadBalanceNodeWeakPtr lb_node;
  std::int32_t cur_weight{0};
  std::int32_t effect_weight{0};
};

class SWRRSBalancer : public ILoadBalancer {
public:
  SWRRSBalancer() = default;
  virtual ~SWRRSBalancer() = default;

public:
  virtual auto add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool override;
  virtual auto get_next(const std::string &ip = "")
      -> LoadBalanceNodeWeakPtr override;
  virtual auto clear() -> void override;
  virtual auto reset_balancer() -> void override{};

  static auto get_name() -> const std::string {
    return "SMOOTH_ROUND_ROBIN_BALANCER";
  }
  static auto get_mod() -> BalancerMod {
    return BalancerMod::Smooth_Weighted_Round_Robin_Scheduling;
  }
  static auto creator() -> std::unique_ptr<ILoadBalancer> {
    return std::make_unique<SWRRSBalancer>();
  }

private:
  static bool smooth_weight_round_robin_register_;
  std::int32_t last_select_index_{-1};
  std::int32_t server_count_{0};
  std::string name_{""};
  std::vector<SWRRSNode> nodes_;
};

struct VNSWRRSNode {
  VNSWRRSNode() = default;
  VNSWRRSNode(LoadBalanceNodeWeakPtr n) : lb_node(n), cur_weight(0) {
    effect_weight = lb_node.lock()->get_weight();
  }
  LoadBalanceNodeWeakPtr lb_node;
  std::int32_t cur_weight{0};
  std::int32_t effect_weight{0};
};

class VNSWRRSBalancer : public ILoadBalancer {
public:
  VNSWRRSBalancer() = default;
  virtual ~VNSWRRSBalancer() = default;

public:
  virtual auto add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool override;
  virtual auto get_next(const std::string &ip = "")
      -> LoadBalanceNodeWeakPtr override;
  virtual auto clear() -> void override;
  virtual auto reset_balancer() -> void override;

  static auto get_name() -> const std::string {
    return "SMOOTH_ROUND_ROBIN_BALANCER";
  }
  static auto get_mod() -> BalancerMod {
    return BalancerMod::Virtual_Node_Smooth_Weighted_Round_Robin_Scheduling;
  }
  static auto creator() -> std::unique_ptr<ILoadBalancer> {
    return std::make_unique<VNSWRRSBalancer>();
  }

private:
  auto get_swrr_next() -> void;
  auto get_virtual_nodes() -> void;

private:
  static bool vn_smooth_weight_round_robin_register_;
  std::int32_t last_select_index_{-1};
  std::int32_t server_count_{0};
  std::int32_t virtual_server_count_{0};
  std::int32_t last_select_virtual_index_{-1};
  std::int32_t tot_weight_{0};
  std::string name_{""};
  std::vector<VNSWRRSNode> nodes_;
  std::vector<std::int32_t> virtual_nodes_;
};

#define Registe_Balancer(mod, func)                                            \
  BalancerFactory::register_balancer(mod, func);

} // namespace loadbalance
} // namespace kratos

#pragma once

//
// COPY FROM:
// https://llvm.org/docs/tutorial
//

#include <map>
#include <memory>
#include <string>
#include <vector>

#include "grammar.h"
#include "kconfig/interface/config.h"
#include "lexer.h"
#include "token.h"

namespace kconfig {

/// ExprAST - Base class for all expression nodes.
class ExprAST {
public:
  virtual ~ExprAST();
  /**
   * Interpret to AttributePtr.
   * 
   * \param lexer lexer
   * \return AttributePtr
   */
  virtual AttributePtr gen_attribute(Lexer *lexer) = 0;
  /**
   * Is empty AST.
   * 
   * \return true or false
   */
  virtual bool isEmpty() = 0;
};

using ExprASTUniPtr = std::unique_ptr<ExprAST>;

class EmptyAST : public ExprAST {
public:
  EmptyAST();
  virtual ~EmptyAST();
  virtual AttributePtr gen_attribute(Lexer* lexer);
  virtual bool isEmpty() { return true; }
};

/// NumberExprAST - Expression class for numeric literals like "1.0".
class NumberExprAST : public ExprAST {
  std::string str_val_;

public:
  NumberExprAST(const std::string &val);
  virtual ~NumberExprAST();
  virtual AttributePtr gen_attribute(Lexer *lexer) override;
  virtual bool isEmpty() override { return false; }
};

class StringExprAST : public ExprAST {
  std::string str_;

public:
  StringExprAST(const std::string &str);
  virtual ~StringExprAST();
  virtual AttributePtr gen_attribute(Lexer *lexer) override;
  virtual bool isEmpty() override { return false; }
};

/// VariableExprAST - Expression class for referencing a variable, like "a".
class VariableExprAST : public ExprAST {
  std::string name_;
  std::string index_;

public:
  VariableExprAST(const std::string &name);
  VariableExprAST(const std::string& name, const std::string& index);
  virtual ~VariableExprAST();
  virtual AttributePtr gen_attribute(Lexer *lexer) override;
  virtual bool isEmpty() override { return false; }
};

/// BinaryExprAST - Expression class for a binary operator.
class BinaryExprAST : public ExprAST {
  int op_{0};
  ExprASTUniPtr lhs_;
  ExprASTUniPtr rhs_;

public:
  BinaryExprAST(int op, ExprASTUniPtr lhs, ExprASTUniPtr rhs);
  virtual ~BinaryExprAST();
  virtual AttributePtr gen_attribute(Lexer *lexer) override;
  virtual bool isEmpty() override { return false; }

private:
  AttributePtr eval(Lexer *lexer, AttributePtr lhs, AttributePtr rhs);
};

/// UniExprAST - Expression class for a uni-operator.
class UniExprAST : public ExprAST {
  int op_{ 0 };
  ExprASTUniPtr rhs_;

public:
  UniExprAST(int op, ExprASTUniPtr rhs);
  virtual ~UniExprAST();
  virtual AttributePtr gen_attribute(Lexer* lexer) override;
  virtual bool isEmpty() override { return false; }

private:
  AttributePtr eval(Lexer* lexer, AttributePtr rhs);
};

/// CallExprAST - Expression class for function calls.
class CallExprAST : public ExprAST {
  std::string callee_;
  std::vector<ExprASTUniPtr> args_;

public:
  CallExprAST(const std::string &callee, std::vector<ExprASTUniPtr> &args);
  virtual ~CallExprAST();
  virtual AttributePtr gen_attribute(Lexer *lexer) override;
  virtual bool isEmpty() override { return false; }

private:
  auto call(Lexer *lexer) -> AttributePtr;
};

/// expression
///   ::= primary binoprhs
///
ExprASTUniPtr ParseExpression(Lexer *lexer);

AttributePtr GenAttribute(ExprASTUniPtr ast, Lexer *lexer);

} // namespace kconfig

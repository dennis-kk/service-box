/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <sstream>

#include "kconfig/interface/config.h"

#include "array_impl.h"
#include "bool_impl.h"
#include "domain.h"
#include "expression.h"
#include "file.h"
#include "file_manager.h"
#include "grammar.h"
#include "lexer.h"
#include "number_impl.h"
#include "string_impl.h"
#include "table_impl.h"
#include "token.h"

using namespace kconfig;

//
// chunk := domain*
// domain := '{' (require | statement | comment | expr | anonydomain)* '}'
// require := 'require' literal
// statement := literal '=' (number | string | domain | expr | array |
// table | expr) comment := ';' (literal)* table := '<' pair (',' pair)* '>'
// pair := key ':' value
// key := literal
// value := array | table | string | number | bool | expr
// array := '(' (array | table | string | number | bool | expr)* ')'
// number := literal
// string := '"' literal '"'
// bool := 'true' | 'false'
// expr := '`' primary_expr | null '`'
//

Grammar::Grammar(Config *config) {
  _root = new Domain("root", 0);
  _fileManager = new FileManager();

  _error = false;
  _base = "";
  _domain = _root;
  _lexer = 0;
  _file = 0;
  _lastError = "";
  _config = config;
}

Grammar::~Grammar() {
  delete _root;
  delete _fileManager;

  _error = false;
  _base = "";
  _root = 0;
  _domain = 0;
  _lexer = 0;
  _file = 0;
  _fileManager = 0;
  _lastError = "";
}

bool Grammar::isOk() { return !_error; }

std::string Grammar::getLastError() {
  std::stringstream reason;

  if (0 != _file) {
    reason << _file->getPath();
  }

  if (0 != _lexer) {
    reason << "(" << _lexer->current()->getRow() << ":"
           << _lexer->current()->getColumn() << "):";
  }

  reason << _lastError;
  return reason.str();
}

Domain *Grammar::get_root_domain() { return _root; }

Config *Grammar::get_config() { return _config; }

void Grammar::error(const std::string &reason) {
  _error = true;
  _lastError = reason;
}

Domain *Grammar::parse(const std::string &base, const std::string &path) {
  _base = base;

  try {
    _file = _fileManager->newFile(_base + path, 0, this);
  } catch (std::runtime_error &) {
    ErrorReturnNull("file not found " + base + path);
  }

  _lexer = _file->getLexer();
  chunk();
  return _root;
}

void Grammar::chunk() {
  Token *token = 0;
  //
  // chunk := domain*
  //
  while ((0 != (token = _lexer->getNext())) && (!_error)) {
    if (token->type() == Token::REQUIRE) { // 'require'
      require();
    } else if (token->type() == Token::LITERAL) { // 'statement'
      statement();
    } else if (token->type() == Token::SLASH_STROPHE) { // 'expression'
      expression_value();
    } else if (token->type() == Token::DOMAIN_START) {
      anonymous_domain(_domain);
    } else {
      if (_domain == _root) { // in top domain
        ErrorReturn("invalid token");
      }

      break;
    }
  }
}

void Grammar::require() {
  //
  // require := 'require' literal
  //
  Token *next = _lexer->getNext();

  if (0 == next) {
    ErrorReturn("need a file name");
  }

  if (next->type() != Token::LITERAL) {
    ErrorReturn("need a file name");
  }

  try {
    // open required file
    _file = _fileManager->newFile(_base + next->getText(), _file, this);
  } catch (std::runtime_error &) {
    ErrorReturn("file not found " + _base + next->getText());
  }

  // set current lexer
  _lexer = _file->getLexer();
  // do chunk
  chunk();

  if (_error) {
    return;
  }

  // up to parent file
  _file = _file->getParent();
  // up to parent lexer
  _lexer = _file->getLexer();
}

void Grammar::expression(Attribute *parent, const std::string &name) {
  _lexer->getNext();
  try {
    auto ast_root(ParseExpression(_lexer));
    if (ast_root) {
      // TRAVEL AST to build attribute, key is name
      if (parent) {
        if (parent->isArray()) {
          dynamic_cast<ArrayImpl *>(parent)->push(
              GenAttribute(std::move(ast_root), _lexer));
        } else if (parent->isTable()) {
          dynamic_cast<TableImpl *>(parent)->add(
              name, GenAttribute(std::move(ast_root), _lexer));
        } else if (parent->isZone()) {
          dynamic_cast<Domain *>(parent)->addAttribute(
              name, GenAttribute(std::move(ast_root), _lexer));
        }
      } else {
        _domain->addAttribute(name, GenAttribute(std::move(ast_root), _lexer));
      }
      if (_lexer->current()->type() != Token::SLASH_STROPHE) {
        ErrorReturn("need a '`'");
      }
    } else {
      ErrorReturn("eval expression failed");
    }
  } catch (std::exception &ex) {
    ErrorReturn(ex.what());
  }
}

void Grammar::expression_value() {
  _lexer->getNext();
  try {
    auto ast_root(ParseExpression(_lexer));
    if (ast_root) {
      // 计算出值
      _cur_expr_val = ast_root->gen_attribute(_lexer);
      if (_lexer->current()->type() != Token::SLASH_STROPHE) {
        ErrorReturn("need a '`'");
      }
    } else {
      ErrorReturn("eval expression failed");
    }
  } catch (std::exception &ex) {
    ErrorReturn(ex.what());
  }
}

void Grammar::domain(const std::string &name) {
  //
  // domain := '{' (require | statement)* '}'
  //
  try {
    // open new domain
    // NOTICE 指针不会失效
    _domain = _domain->newChild(name).get();
  } catch (std::runtime_error &exc) {
    ErrorReturn(exc.what());
  }

  // do chunk
  chunk();

  if (_error) {
    return;
  }

  Token *token = _lexer->current();

  if (0 == token) {
    ErrorReturn("need a '}'");
  }

  if (token->type() != Token::DOMAIN_END) {
    ErrorReturn("need a '}'");
  }

  // up to parent domain
  _domain = _domain->getParent();
}

void Grammar::anonymous_domain(Domain *parent_domain) {
  if (!_cur_expr_val) {
    ErrorReturn("need expr");
  }
  if (!_cur_expr_val->isBool()) {
    ErrorReturn("expr must has bool result");
  }
  bool skip = !_cur_expr_val->boolean()->get();

  // 如果是条件配置，不需要解析，直接跳过整个匿名domain
  if (skip) {
    int level = 1;
    while (0 != level) {
      auto *next_tok = _lexer->getNext();
      if (next_tok->type() == Token::DOMAIN_START) {
        level += 1;
      } else if (next_tok->type() == Token::DOMAIN_END) {
        level -= 1;
      }
    }
    return;
  }

  //
  // domain := '{' (require | statement)* '}'
  //

  auto domain_ptr = std::make_shared<Domain>("", parent_domain);
  // 当前域为匿名域
  _domain = domain_ptr.get();

  // do chunk
  chunk();

  if (_error) {
    return;
  }

  auto *token = _lexer->current();

  if (0 == token) {
    ErrorReturn("need a '}'");
  }

  if (token->type() != Token::DOMAIN_END) {
    ErrorReturn("need a '}'");
  }

  parent_domain->merge_from(domain_ptr.get());
  _domain = parent_domain;
  _cur_expr_val = nullptr;
}

void Grammar::array(Attribute *parent, const std::string &name) {
  //
  // array := '(' (array | table | string | number | expression)* ')'
  //
  auto arrayImpl = std::make_shared<ArrayImpl>();

  if (0 != parent) {
    if (parent->isArray()) {
      // push to array
      dynamic_cast<ArrayImpl *>(parent)->push(arrayImpl);
    } else if (parent->isTable()) {
      try {
        // insert to table
        dynamic_cast<TableImpl *>(parent)->add(name, arrayImpl);
      } catch (std::runtime_error &exc) {
        ErrorReturn(exc.what());
      }
    }
  } else {
    try {
      // add to domain
      _domain->addAttribute(name, arrayImpl);
    } catch (std::runtime_error &exc) {
      ErrorReturn(exc.what());
    }
  }

  //
  // do array element
  //
  Token *next = _lexer->getNext();

  while ((0 != next) && (next->type() != Token::ARRAY_END) && (!_error)) {
    if (next->type() == Token::ARRAY_START) {
      array(arrayImpl.get(), ""); // 'array'
    } else if (next->type() == Token::TABLE_START) {
      table(arrayImpl.get(), ""); // 'table'
    } else if (next->type() == Token::QUOTE) {
      string(arrayImpl.get(), ""); // 'string'
    } else if (next->type() == Token::SLASH_STROPHE) {
      expression(arrayImpl.get(), ""); // 'expression'
    } else if (next->type() == Token::LITERAL) {
      if (next->getText() == "true" || (next->getText() == "false")) {
        bool_literal(arrayImpl.get(), "", next->getText()); // 'bool'
      } else {
        number(arrayImpl.get(), "", next->getText()); // 'number'
      }
    } else {
      ErrorReturn("not a valid array element"); // un-supported element type
    }

    next = _lexer->getNext();

    if (0 == next) {
      ErrorReturn("need a ',' or ')'");
    }

    if ((next->type() != Token::COMMA) && (next->type() != Token::ARRAY_END)) {
      ErrorReturn("need a ',' or ')'");
    }

    if (next->type() == Token::ARRAY_END) {
      return;
    }

    // next token
    next = _lexer->getNext();
  }

  if (_error) {
    return;
  }

  if (next->type() != Token::ARRAY_END) {
    ErrorReturn("need a ')'");
  }
}

void Grammar::pair(Attribute *parent) {
  //
  // pair := key ':' value
  // key := literal
  // value := array | table | string | number | expression
  //
  Token *token = _lexer->current();
  std::string key;

  if (token->type() == Token::LITERAL) {
    key = token->getText();
  } else {
    ErrorReturn("need a literal key for table pair");
  }

  token = _lexer->getNext();

  if (token->type() != Token::COLON) {
    ErrorReturn("need a ':'");
  }

  token = _lexer->getNext();

  if (token->type() == Token::LITERAL) {
    if (token->getText() == "true" || (token->getText() == "false")) {
      bool_literal(parent, key, token->getText()); // 'bool'
    } else {
      number(parent, key, token->getText()); // 'number'
    }
  } else if (token->type() == Token::QUOTE) {
    string(parent, key); // 'string'
  } else if (token->type() == Token::ARRAY_START) {
    array(parent, key); // 'array'
  } else if (token->type() == Token::TABLE_START) {
    table(parent, key); // 'table'
  } else if (token->type() == Token::SLASH_STROPHE) {
    expression(parent, key); // 'expression'
  } else {
    ErrorReturn("not a valid table element");
  }
}

void Grammar::table(Attribute *parent, const std::string &name) {
  //
  // table := '<' pair (',' pair)* '>'
  //
  auto tableImpl = std::make_shared<TableImpl>();

  if (0 != parent) {
    if (parent->isArray()) {
      dynamic_cast<ArrayImpl *>(parent)->push(tableImpl);
    } else if (parent->isTable()) {
      try {
        dynamic_cast<TableImpl *>(parent)->add(name, tableImpl);
      } catch (std::runtime_error &exc) {
        ErrorReturn(exc.what());
      }
    }
  } else {
    try {
      _domain->addAttribute(name, tableImpl);
    } catch (std::runtime_error &exc) {
      ErrorReturn(exc.what());
    }
  }

  Token *token = _lexer->getNext();

  while ((0 != token) && (token->type() != Token::TABLE_END) && (!_error)) {
    pair(tableImpl.get());
    token = _lexer->getNext();

    if (0 == token) {
      ErrorReturn("need a ',' or '>'");
    }

    if ((token->type() != Token::COMMA) &&
        (token->type() != Token::TABLE_END)) {
      ErrorReturn("need a ',' or '>'");
    }

    if (token->type() == Token::TABLE_END) {
      break;
    }

    token = _lexer->getNext();
  }

  if (_error) {
    return;
  }

  if (token->type() != Token::TABLE_END) {
    ErrorReturn("need a '>'");
  }
}

void Grammar::string(Attribute *parent, const std::string &key) {
  //
  // string := '"' literal '"'
  //
  Token *token = _lexer->getNext();

  if (0 == token) {
    ErrorReturn("need a string");
  }

  std::string value;

  if (token->type() == Token::LITERAL) {
    value = token->getText();
  } else {
    if (token->type() == Token::QUOTE) {
      value = "";
    } else {
      ErrorReturn("need a string");
    }
  }

  auto stringImpl = std::make_shared<StringImpl>(value);

  if (0 != parent) {
    if (parent->isArray()) {
      dynamic_cast<ArrayImpl *>(parent)->push(stringImpl);
    } else if (parent->isTable()) {
      try {
        dynamic_cast<TableImpl *>(parent)->add(key, stringImpl);
      } catch (std::runtime_error &exc) {
        ErrorReturn(exc.what());
      }
    }
  } else {
    try {
      // add to domain
      _domain->addAttribute(key, stringImpl);
    } catch (std::runtime_error &exc) {
      ErrorReturn(exc.what());
    }
  }

  // empty string
  if (value.empty()) {
    return;
  }

  token = _lexer->getNext();

  if (0 == token) {
    ErrorReturn("need a '\"'");
  }

  if (token->type() != Token::QUOTE) {
    ErrorReturn("need a '\"'");
  }
}

void Grammar::number(Attribute *parent, const std::string &key,
                     const std::string &name) {
  //
  // number := literal
  //
  if (0 != parent) {
    if (parent->isArray()) {
      auto numberImpl = std::make_shared<NumberImpl>(name);
      dynamic_cast<ArrayImpl *>(parent)->push(numberImpl);
    } else if (parent->isTable()) {
      auto numberImpl = std::make_shared<NumberImpl>(name);

      try {
        dynamic_cast<TableImpl *>(parent)->add(key, numberImpl);
      } catch (std::runtime_error &exc) {
        ErrorReturn(exc.what());
      }
    } else {
      ErrorReturn("need a container");
    }
  } else {
    auto numberImpl = std::make_shared<NumberImpl>(name);

    try {
      _domain->addAttribute(key, numberImpl);
    } catch (std::runtime_error &exc) {
      ErrorReturn(exc.what());
    }
  }
}

void Grammar::bool_literal(Attribute *parent, const std::string &key,
                           const std::string &name) {
  //
  // bool := true | false
  //
  if (0 != parent) {
    if (parent->isArray()) {
      auto boolImpl = std::make_shared<BoolImpl>(name == "true" ? true : false);
      dynamic_cast<ArrayImpl *>(parent)->push(boolImpl);
    } else if (parent->isTable()) {
      auto boolImpl = std::make_shared<BoolImpl>(name == "true" ? true : false);
      try {
        dynamic_cast<TableImpl *>(parent)->add(key, boolImpl);
      } catch (std::runtime_error &exc) {
        ErrorReturn(exc.what());
      }
    } else {
      ErrorReturn("need a container");
    }
  } else {
    auto boolImpl = std::make_shared<BoolImpl>(name == "true" ? true : false);

    try {
      _domain->addAttribute(key, boolImpl);
    } catch (std::runtime_error &exc) {
      ErrorReturn(exc.what());
    }
  }
}

void Grammar::statement() {
  //
  // statement := literal '=' (number | string | domain | array | table |
  // expression)
  //
  Token *current = _lexer->current();
  Token *next = _lexer->getNext();

  if (0 == next) {
    ErrorReturn("need a '='");
  }

  if (next->type() != Token::EQUAL) {
    ErrorReturn("need a '='");
  }

  next = _lexer->getNext();

  if (0 == next) {
    ErrorReturn("need a 'statement'");
  }

  if (next->type() == Token::LITERAL) {
    if (next->getText() == "true" || (next->getText() == "false")) {
      bool_literal(0, current->getText(), next->getText());
    } else {
      number(0, current->getText(), next->getText());
    }
  } else if (next->type() == Token::QUOTE) {
    string(nullptr, current->getText());
  } else if (next->type() == Token::DOMAIN_START) {
    domain(current->getText());
  } else if (next->type() == Token::ARRAY_START) {
    array(nullptr, current->getText());
  } else if (next->type() == Token::TABLE_START) {
    table(nullptr, current->getText());
  } else if (next->type() == Token::SLASH_STROPHE) {
    expression(nullptr, current->getText());
  }
}

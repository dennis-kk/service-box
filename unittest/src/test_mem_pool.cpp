#include "box/fixed_mem_pool.hh"
#include "../framework/unittest.hh"

FIXTURE_BEGIN(test_mem_pool)

using namespace kratos;

SETUP([]() {});

CASE(TestRent1) {
  auto *p = MempoolRef.rent(1);
  MempoolRef.recycle(p);
  auto *p1 = MempoolRef.rent(1);
  ASSERT_TRUE(p == p1);
  ASSERT_TRUE(MempoolRef.recycle(p));
}

CASE(TestRent2) {
  // Big memory
  auto *p = MempoolRef.rent(1024 * 16 + 1);
  ASSERT_TRUE(MempoolRef.recycle(p));
}

CASE(TestRent3) {
  auto *p = MempoolRef.rent(3455);
  ASSERT_TRUE(MempoolRef.recycle(p));

  p = MempoolRef.rent(433);
  ASSERT_TRUE(MempoolRef.recycle(p));

  p = MempoolRef.rent(33);
  ASSERT_TRUE(MempoolRef.recycle(p));

  p = MempoolRef.rent(78);
  ASSERT_TRUE(MempoolRef.recycle(p));
}

CASE(TestRecycle1) {
  auto *p = MempoolRef.rent(1);
  ASSERT_TRUE(MempoolRef.recycle(p));
  ASSERT_EXCEPT(MempoolRef.recycle(p));
}

TEARDOWN([]() {});

FIXTURE_END(test_mem_pool)

#!/bin/sh

yum install gcc dos2unix -y

if [ ! -f /usr/bin/cc ];then 
    ln -s /usr/local/gcc/bin/gcc /usr/bin/cc
fi

python build_local_env_linux.py

dos2unix *

chmod +x ./initgcc.sh
chmod +x ./build_repo_linux.sh
chmod +x ../tools/start_linux_box.sh
chmod +x ../tools/start_linux_local_env.sh
chmod +x ../tools/stop_linux_local_env.sh

dos2unix ./service-box-linux-dependency/zookeeper-bin/bin/*
chmod +x ./service-box-linux-dependency/zookeeper-bin/bin/zkServer.sh

wget https://cmake.org/files/v3.10/cmake-3.10.0.tar.gz
tar zxvf cmake-3.10.0.tar.gz
cd cmake-3.10.0
./bootstrap
gmake -j8
make install
cd ..
rm cmake-3.10.0 -rf
rm cmake-3.10.0.tar.gz -f

cd ../src/repo
python repo.py -t cpp -i
python repo.py -t cpp -a example/example.idl
python repo.py -t cpp -b example
python repo.py -t lua -i
python repo.py -t lua -u example
python repo.py -t lua -b example

cd ..
cmake .
make -j4

cd ../unittest/src
cmake .
make -j4
# -*- coding: UTF-8 -*-

import zipfile
import os
import platform
import shutil
import sys

def_tag='develop'

class LinuxEnv:
    def __init__(self):
        pass

    def check_remove(self):
        if os.path.exists('service-box-linux-dependency'):
            os.system("rm -rf service-box-linux-dependency")
        return True
    
    def pull_dependency(self):
        if os.system("git clone https://gitee.com/dennis-kk/service-box-linux-dependency --depth=1") != 0:
            print("Pull linux dependency failed")
            return False
        return True

    def build(self):
        if platform.system() != "Linux" and platform.system() != "Darwin":
            print("Only run on linux")
            return False
            
        # if os.system("java -version") != 0:
        #    print("Please install JDK")
        #    return False
        
        if not (self.check_remove() and self.pull_dependency()):
            return False
        
        zip = zipfile.ZipFile('./service-box-linux-dependency/service-box-linux-dependency.zip', 'r')
        for file in zip.namelist(): 
            zip.extract(file, "./service-box-linux-dependency/")
        zip.close()

        return True

if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print("build service box local env with default branch: ", def_tag)
    else:
        def_tag=sys.argv[1] 
        print("[service-box] build local env with input branch: ", def_tag)

    env = LinuxEnv()
    if not env.build():
        exit(2)

        
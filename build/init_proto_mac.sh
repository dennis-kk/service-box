#!/bin/bash

#MIT License
#
#Copyright (c) 2023 cloudguan rcloudguan@163.com
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

# 指向 repo/tmp 目录
CURPATH=`pwd`

#前置环境安装，编译需要旧环境
function initenv() {
    echo ============init protoc================
    sudo $ISN install libtool -y 
    sudo $ISN install autoconf -y 
}

function down_protobuf(){
    echo "protobuff 开始下载 版本3.10.0"
    wget https://github.com/protocolbuffers/protobuf/archive/v3.10.0.tar.gz
    if [ $? -gt 0 ]
    then
        echo "下载失败请手动下载protobuf 在执行脚本"
        exit -1
    fi
}

function install_proto(){
    echo "开始解压"
    if [ ! -d ./protobuf-3.10.0 ]
    then
        tar zxvf v3.10.0.tar.gz
        if [ $? -gt -0 ]
        then
            echo "解压失败 请检查文件是否正确"
            exit 1
        fi 
    fi

    cd protobuf-3.10.0

    ./autogen.sh

    if [ -d ./mac_build ]; then
        rm -rf mac_build
    fi
    mkdir mac_build

    cd mac_build
    echo "开始配置 ../configure --prefix=/usr/local/protobuf --enable-shared CXXFLAGS=-fPIC"
    ../configure --bindir=${CURPATH}/../thirdparty/bin --includedir=${CURPATH}/../thirdparty/include --libdir=${CURPATH}/../thirdparty/lib/linux --enable-shared CXXFLAGS="-fPIC"

    if [ $? -gt 0 ]
    then
        echo "--prefix=/usr/local/protobuf --enable-shared CXXFLAGS=-fPIC 执行失败，检查log文件"
        exit 1
    fi 

    make -j4
    make install 

    if [ $? -gt 0 ]
    then
        echo "install protobuff failed!!"
        exit 1
    fi 

    echo "replace system protoc with ourself's"
    if [ -L /usr/local/bin/protoc ];then 
        sudo rm /usr/local/bin/protoc
    fi 
    sudo ln -s ${CURPATH}/../thirdparty/bin/protoc /usr/local/bin/protoc
}

#check protobuff version and replace with v3.10.0
if [ -x "$(command -v protoc)" ]; then
### get version code
echo "已经安装protoc 请自行确定版本是否正确 最小版本 3.10"
##echo `protoc --version`
fi

#check current dir
echo "cur dir : ${CURPATH}" 

# init 
# initenv

# 切换到 tmp 目录
cd ../../

echo "开始下载proto 文件"
if [ ! -f v3.10.0.tar.gz ]
then
    down_protobuf
fi

install_proto


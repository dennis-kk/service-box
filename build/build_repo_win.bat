cd ..\src\repo
python init.py
python repo.py -t cpp -i
python repo.py -t cpp -a example\example.idl
python repo.py -t cpp -b example
python repo.py -t lua -i
python repo.py -t lua -u example
python repo.py -t lua -b example

cd ..\..\win-proj
"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild" service-box.sln -target:service-box
"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild" service-box.sln -target:service-box-unittest
"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild" /P:Configuration=Release service-box.sln -target:service-box
"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild" /P:Configuration=Release service-box.sln -target:service-box-unittest


cd ..\src\repo
copy /y lib\stub\example\ServiceDynamic\Debug\libServiceDynamic.so ..\..\build\service-box-windows-dependency\nginx\html\doc

pause
exit

@ :func
  python repo.py %1 %2 %3 %4 %5 %6 %7 %8 %9
@ if %errorlevel% == 0 ( echo "Success!" ) else ( echo "Build Faild !" & pause )
@ goto:eof

#!/bin/bash

#MIT License
#
#Copyright (c) 2023 cloudguan rcloudguan@163.com
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

#初始化安装protoc, 检查的是box目录下
if [ ! -f ../thirdparty/bin/protoc ]; then
    bash init_proto_mac.sh
    if [ $? -gt 0 ];then
        echo "install protobuf error"
        exit 1
    fi
fi

#准备相关第三方库依赖
#python build_local_env_linux.py

dos2unix *

#chmod +x ./initgcc.sh
#chmod +x ./build_repo_linux.sh
#chmod +x ../tools/start_linux_box.sh
#chmod +x ../tools/start_linux_local_env.sh
#chmod +x ../tools/stop_linux_local_env.sh

dos2unix ./service-box-linux-dependency/zookeeper-bin/bin/*
chmod +x ./service-box-linux-dependency/zookeeper-bin/bin/zkServer.sh

cd service-box-linux-dependency

if [ ! -d ../../thirdparty/include/hiredis ];then
	mkdir -p ../../thirdparty/include/hiredis
fi

if [ ! -d ../../thirdparty/lib/linux ];then
	mkdir -p ../../thirdparty/lib/linux
fi

echo "copy thirdparty lib to framework!!"
cp -r hiredis/ ../../thirdparty/include/hiredis
cp libcurl.a ../../thirdparty/lib/linux/libcurl.a
cp libhashtable.a ../../thirdparty/lib/linux/libhashtable.a
cp libhiredis.a ../../thirdparty/lib/linux/libhiredis.a
#cp libz.a ../../thirdparty/lib/linux/libz.a  -rf
cp libzookeeper.a ../../thirdparty/lib/linux/libzookeeper.a

#设置默认路径,如果用户没有输入的话
INSTALL_PATH='../framework'
if [ $1 ];then
INSTALL_PATH=$1
fi 

if [ -d ../../cmakeproj ];then
	rm -rf ../../cmakeproj/*
else
	mkdir ../../cmakeproj
fi 

cd ../../cmakeproj
echo "install path: ${INSTALL_PATH}"

if [ -d debug ];then
	rm -rf ./debug/*
else
	mkdir debug
fi

CXXNAME='g++'
CNAME='gcc'

if command -v clang++ &> /dev/null
then
    CNAME='clang'
    CXXNAME='clang++'
fi

cd debug
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=../${INSTALL_PATH} -DCMAKE_C_COMPILER=${CNAME} -DCMAKE_CXX_COMPILER=${CXXNAME} ../../src
# make -j16 && make install

cd ..
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH} -DCMAKE_C_COMPILER=${CNAME} -DCMAKE_CXX_COMPILER=${CXXNAME} ../src
# make -j16 && make install


# Service Box

```
 _____   _____  __    __ 
|  _  \ /  _  \ \ \  / / 
| |_| | | | | |  \ \/ /  
|  _  { | | | |   }  {   
| |_| | | |_| |  / /\ \  
|_____/ \_____/ /_/  \_\ 
```
Service Box(服务容器)是一个C++(std=C++17)语言编写的组件容器系统，组件以DLL/SO的方式被容器加载运行，容器做为组件的底层支撑提供必要的跨平台的功能，同时组件可以将通过“注册”将自己暴露在由多个服务容器组成的分布式集群，不同容器内的组件通过“获取”来建立和其他组件的联系，但不需要关心服务提供方在哪个服务容器内，部署在哪台物理/虚拟硬件上，组件间的通信采用RPC(Remote Procedure Call)，用户不需要关心协议，只需要获取组件的代理接口并调用方法即可。用户的设计以组件接口为最小单元，不同的组件间交互只需要阅读对方的组件接口说明，有利于功能间互调用的解耦，更能促使开发者从设计角度去考虑团队合作。

1. [安装](README-install.md)
2. [发布](publish)
4. [编写C++服务](README-cpp_service.md)
    1. [C++ API文档](src/box/README-service_context.md)
    2. [异步调用说明](README-cpp_async_proxy_call.md)
    3. [同步调用说明](README-cpp_sync_proxy_call.md)
    4. [C++服务的协程化调用](README-cpp_coroutine.md)
5. [编写Lua服务](README-lua_service.md)
    1. [Lua API文档](src/util/lua/README.md)
    2. [同步调用说明](README-lua_sync_proxy_call.md)
5. Demo Quick Start
    1.  [Cpp Quick Start](README-cpp_quick_start.md)
    2.  [Lua Quick Start](README-lua_quick_start.md)
6. Web控制台<br>
   默认容器启动后，使用chrome访问<br>
   http://localhost:6889/root, 参考[Web控制台](src/console/README.md)
7. [工程结构](README-project_strcutre.md)
8. [配置](README-configuration.md)
9. [代理模式启动](README-proxy_mode.md)
10. [C# RPC(客户端)](README-csharp.md)
11. [一键生成mock使用](README-mock.md)


# Lua proxy同步调用

使用Lua编写服务时支持lua协程的方式与远程service实例进行RPC通信, lua协程的启动和调度已经内置在service box内部，对使用者没有感知.

# 调用过程

假设有如下服务:

```
service dynamic Login multiple=8 {
    ui64 login(string, string)
    void logout(ui64)
}
```
框架生成工具会生成如下的lua proxy文件, 具体实现已省略:
```
ProxyLogin={}
ProxyLogin.proxy_id = 0
EmptyArgs = {}

function ProxyLogin:login(arg1,arg2)
    ......
end

function ProxyLogin:logout(arg1)
    ......
end

Proxy = Proxy or {}

function Proxy:get_Login_timeout(name, timeout)
    ......
end
......
```
调用方服务实现内, 假设注册的服务路径为'/service/login', 调用Login服务代理的异步版本:
```
--获取服务，可保存以备后续使用
--获取名字为/service/login的服务，超时时间为2000毫秒，超时将返回nil
local login_prx = Proxy:get_LuaLogin_timeout("/service/login", 2000)
......
--通过代理调用远程服务实例，过程中协程会yield，调用失败或超时将返回nil
local id = login_prx:login("user", "passwd")
```

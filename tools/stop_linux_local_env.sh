#!/bin/sh

nginx -s stop
redis-cli shutdown
cd ../build/service-box-linux-dependency/zookeeper-bin/bin
./zkServer.sh stop

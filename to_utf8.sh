#!/bin/sh

find . -name "*.cpp" -exec file -i {} \; > cppencode
find . -path ./thirdparty -prune -o -name "*.h" -exec file -i {} \; >> cppencode
find . -path ./thirdparty -prune -o -name "*.hh" -exec file -i {} \; >> cppencode
grep iso-8859-1 cppencode | awk -F: '{print $1}' > convert_wait
filelist=`cat convert_wait`
count=1
for afile in $filelist
do
    echo convert file : $afile
    echo -ne '\xEF\xBB\xBF' > utf8bom
    iconv -f GB2312 -t UTF-8//TRANSLIT -s  $afile >> utf8bom
    cat utf8bom > $afile
    rm utf8bom
    echo =
done
rm cppencode -f
rm convert_wait -f